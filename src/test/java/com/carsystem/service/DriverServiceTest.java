package com.carsystem.service;

import com.carsystem.model.jsonrequestbody.PageJson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DriverServiceTest {

    @Resource
    private DriverService driverService;

    @Test
    public void queryDriverInfo() {

    }

    @Test
    public void driverQueryRequest() {
//        System.out.print(driverService.driverQueryRequest("16121666",2)+"\n");
    }

    @Test
    public void driverQueryRequestInfo() {
        PageJson pageJson = new PageJson();
        pageJson.setPage(1);
//        System.out.print(driverService.driverQueryRequestInfo("16121666",pageJson)+"\n");
    }

    @Test
    public void insertCarRequest() {
    }

    @Test
    public void queryCarRequest() {
    }

    @Test
    public void queryCarRequestInfo() {
    }
}