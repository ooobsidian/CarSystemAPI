package com.carsystem.tools;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class JwtUtilTest {

    @Test
    public void createJwt() {
        System.out.println(JwtUtil.createJwt("14122602"));
    }


    @Test
    public void parseJwt() {
        Assert.assertEquals("14122602", JwtUtil.parseJwt(JwtUtil.createJwt("14122602")));
    }
}