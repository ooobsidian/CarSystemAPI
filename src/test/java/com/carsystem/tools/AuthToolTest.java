package com.carsystem.tools;

import com.carsystem.model.entity.SystemUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AuthToolTest {

    @Test
    public void getAuth() {
        Assert.assertFalse(AuthTool.getAuth("14122602", "XXX"));
    }

    @Test
    public void getInfo() {
        SystemUser user = AuthTool.getInfo("14122602");
        Assert.assertNotNull(user);
    }


}