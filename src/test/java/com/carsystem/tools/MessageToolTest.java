package com.carsystem.tools;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.carsystem.model.ShortMessage.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MessageToolTest {


    //向乘客发送预约成功短信
    @Test
    public void successMessageToPassenger() throws ClientException {
        SuccessMessage successMessage = new SuccessMessage();
        successMessage.setPassengerName("文满叶");
        successMessage.setDriverName("徐文");
        successMessage.setPlace("新世纪");
        successMessage.setBeginTime(MessageTool.transferLongDateToString(201811251330L));
        successMessage.setEndTime(MessageTool.transferLongDateToString(201811251500L));
        successMessage.setDriverPhone("18817602319");
        successMessage.setPassengerPhone("18817602319");
        SendSmsResponse sendSmsResponse = MessageTool.successMessageToPassenger(successMessage, "18817602319");
        Assert.assertEquals("OK", sendSmsResponse.getCode());
    }

    //向乘客发送预约失败消息
    @Test
    public void FailMessageToPassenger() throws ClientException {
        FailMessage failMessage = new FailMessage();
        failMessage.setPassengerName("文满叶");
        failMessage.setBeginTime(MessageTool.transferLongDateToString(201811251330L));
        failMessage.setEndTime(MessageTool.transferLongDateToString(201811251500L));
        failMessage.setPassengerPhone("18817602319");
        SendSmsResponse sendSmsResponse = MessageTool.FailMessageToPassenger(failMessage,"18817602319");

    }

    //向管理员发送预约提交提醒消息
    @Test
    public void RemindMessageToAdmin() throws ClientException{
        RequestRemind requestRemind = new RequestRemind();
        Date date = new Date();
        requestRemind.setPassengerId("16121666");
        requestRemind.setRequestTime(MessageTool.transferLongDateToString(201811251330L));
        SendSmsResponse sendSmsResponse = MessageTool.RemindMessageToAdmin(requestRemind,"18817602319");
    }

    //向司机发送预约成功消息
    @Test
    public void successMessageToDriver() throws ClientException{
        SuccessMessage successMessage = new SuccessMessage();
        successMessage.setPassengerName("文满叶");
        successMessage.setDriverName("徐文");
        successMessage.setPlace("新世纪");
        successMessage.setBeginTime(MessageTool.transferLongDateToString(201811251330L));
        successMessage.setEndTime(MessageTool.transferLongDateToString(201811251500L));
        successMessage.setDriverPhone("18817602319");
        successMessage.setPassengerPhone("18817602319");
        SendSmsResponse sendSmsResponse = MessageTool.successMessageToDriver(successMessage, "18817602319");
    }

    //向管理员发送司机请假申请提交提醒消息
    @Test
    public void RemindCarMessageToAdmin() throws ClientException{
        CarRequestRemind carRequestRemind = new CarRequestRemind();
        carRequestRemind.setDriverId("16121666");
        carRequestRemind.setDriverName("徐文");
        carRequestRemind.setDate(MessageTool.transferLongDateToString(201811251330L));
        carRequestRemind.setCarId(1);
        MessageTool.RemindCarMessageToAdmin(carRequestRemind,"18817602319");
    }

    //向司机发送请假申请审核通过通知
    @Test
    public void SuccessCarMessageToAdmin() throws ClientException{
        CarRequestMessage carRequestMessage = new CarRequestMessage();
        carRequestMessage.setDriverName("徐文");
        carRequestMessage.setCarId(1);
        carRequestMessage.setDate(MessageTool.transferLongDateToString(201811251330L));
        MessageTool.SuccessCarMessageToAdmin(carRequestMessage,"18817602319");
    }

    //向司机发送请假申请审核不通过通知
    @Test
    public void FailCarMessageToAdmin() throws ClientException{
        CarRequestMessage carRequestMessage = new CarRequestMessage();
        carRequestMessage.setDriverName("徐文");
        carRequestMessage.setCarId(1);
        carRequestMessage.setDate(MessageTool.transferLongDateToString(201811251330L));
        MessageTool.FailCarMessageToAdmin(carRequestMessage,"18817602319");
    }

    @Test
    public void sendCode() throws ClientException {
        CodeMessage codeMessage = new CodeMessage();
        codeMessage.setCode(MessageTool.generateCode());
        MessageTool.sendCode(codeMessage, "18817602319");
    }

    @Test
    public void generateCode() {
        System.out.println(MessageTool.generateCode());
        System.out.println(UUID.randomUUID());
    }
}