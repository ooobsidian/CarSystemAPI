package com.carsystem.dao;

import com.carsystem.CarsystemApplication;
import com.carsystem.model.entity.SystemRequest;
import com.carsystem.model.entity.SystemRequestExample;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CarsystemApplication.class)
public class SystemRequestMapperTest {

    @Resource
    private SystemRequestMapper systemRequestMapper;

    @Test
    public void selectByExample() {
        Integer page = 2;
        SystemRequestExample example = new SystemRequestExample();
        example.createCriteria()
                .andPassengerIdEqualTo("16121670");
        List<SystemRequest> requests = new ArrayList<>();
        requests = systemRequestMapper.selectByExample((page - 1) * 10,example);
        System.out.print(requests);
    }

    @Test
    public void countByExample() {
        SystemRequestExample example = new SystemRequestExample();
        example.createCriteria()
                .andPassengerIdLike("16121670");
        int a = systemRequestMapper.countByExample(example);
        System.out.print(a);
    }

    @Test
    public void selectByPrimaryKey() {
        long a = 63L;
        System.out.print(systemRequestMapper.selectByPrimaryKey(a));
    }

}