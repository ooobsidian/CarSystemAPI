package com.carsystem.dao;

import com.carsystem.CarsystemApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CarsystemApplication.class)
public class SystemCarMapperTest {
    @Resource
    private SystemCarMapper carMapper;

    @Test
    public void getAvaliableCar() {
        Assert.assertEquals(0, carMapper.getAvaliableCar(201805030700L, 201805030900L).size());
    }
}