package com.carsystem.dao;

import com.carsystem.model.entity.SystemRequest;
import com.carsystem.model.entity.SystemRequestExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemRequestMapper {
    int countByExample(@Param("example") SystemRequestExample example);

    int deleteByExample(@Param("example") SystemRequestExample example);

    int deleteByPrimaryKey(Long requestId);

    int insert(SystemRequest record);

    int insertSelective(SystemRequest record);

    List<SystemRequest> selectByExample(@Param("page")Integer page, @Param("example") SystemRequestExample example);
    List<SystemRequest> select(@Param("example") SystemRequestExample example);

    SystemRequest selectByPrimaryKey(Long requestId);

    int updateByExampleSelective(@Param("record") SystemRequest record, @Param("example") SystemRequestExample example);

    int updateByExample(@Param("record") SystemRequest record, @Param("example") SystemRequestExample example);

    int updateByPrimaryKeySelective(SystemRequest record);

    int updateByPrimaryKey(SystemRequest record);
}