package com.carsystem.dao;

import com.carsystem.model.entity.SystemNotice;
import com.carsystem.model.entity.SystemNoticeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemNoticeMapper {
    int countByExample(SystemNoticeExample example);

    int deleteByExample(SystemNoticeExample example);

    int deleteByPrimaryKey(Integer noticeId);

    int insert(SystemNotice record);

    int insertSelective(SystemNotice record);

    List<SystemNotice> selectByExample(SystemNoticeExample example);

    SystemNotice selectByPrimaryKey(Integer noticeId);

    int updateByExampleSelective(@Param("record") SystemNotice record, @Param("example") SystemNoticeExample example);

    int updateByExample(@Param("record") SystemNotice record, @Param("example") SystemNoticeExample example);

    int updateByPrimaryKeySelective(SystemNotice record);

    int updateByPrimaryKey(SystemNotice record);
}