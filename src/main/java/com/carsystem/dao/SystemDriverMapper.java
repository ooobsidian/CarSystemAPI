package com.carsystem.dao;

import com.carsystem.model.entity.SystemDriver;
import com.carsystem.model.entity.SystemDriverExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemDriverMapper {
    int countByExample( @Param("example") SystemDriverExample example);

    int deleteByExample( @Param("example") SystemDriverExample example);

    int deleteByPrimaryKey(String driverId);

    int insert(SystemDriver record);

    int insertSelective(SystemDriver record);

    List<SystemDriver> selectByExampleAndPage( @Param("example") SystemDriverExample example, @Param("page") Integer page);

    List<SystemDriver> selectByExample( @Param("example") SystemDriverExample example);

    SystemDriver selectByPrimaryKey(String driverId);

    int updateByExampleSelective(@Param("record") SystemDriver record, @Param("example") SystemDriverExample example);

    int updateByExample(@Param("record") SystemDriver record, @Param("example") SystemDriverExample example);

    int updateByPrimaryKeySelective(SystemDriver record);

    int updateByPrimaryKey(SystemDriver record);
}