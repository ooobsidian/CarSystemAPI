package com.carsystem.dao;

import com.carsystem.model.entity.SystemCarRequest;
import com.carsystem.model.entity.SystemCarRequestExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemCarRequestMapper {
    int countByExample(@Param("example") SystemCarRequestExample example);

    int deleteByExample(@Param("example") SystemCarRequestExample example);

    int deleteByPrimaryKey(Long carRequestId);

    int insert(SystemCarRequest record);

    int insertSelective(SystemCarRequest record);

    List<SystemCarRequest> selectByExample(@Param("example") SystemCarRequestExample example,@Param("page")Integer page);

    SystemCarRequest selectByPrimaryKey(Long carRequestId);

    int updateByExampleSelective(@Param("record") SystemCarRequest record, @Param("example") SystemCarRequestExample example);

    int updateByExample(@Param("record") SystemCarRequest record, @Param("example") SystemCarRequestExample example);

    int updateByPrimaryKeySelective(SystemCarRequest record);

    int updateByPrimaryKey(SystemCarRequest record);
}