package com.carsystem.dao;

import com.carsystem.model.entity.SystemPassenger;
import com.carsystem.model.entity.SystemPassengerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemPassengerMapper {
    int countByExample(@Param("example")SystemPassengerExample example);

    int deleteByExample(@Param("example") SystemPassengerExample example);

    int insert(SystemPassenger record);

    int insertSelective(SystemPassenger record);

    List<SystemPassenger> selectByExample(@Param("example") SystemPassengerExample example,@Param("page")Integer page);

    SystemPassenger selectByUserId(String userId);

    int updateByExampleSelective(@Param("record") SystemPassenger record, @Param("example") SystemPassengerExample example);

    int updateByExample(@Param("record") SystemPassenger record, @Param("example") SystemPassengerExample example);

}