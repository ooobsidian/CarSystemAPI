package com.carsystem.dao;

import com.carsystem.model.entity.SystemCar;
import com.carsystem.model.entity.SystemCarExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemCarMapper {
    int countByExample(@Param("example") SystemCarExample example);

    int deleteByExample(@Param("example") SystemCarExample example);

    int deleteByPrimaryKey(Integer carId);

    int insert(SystemCar record);

    int insertSelective(SystemCar record);

    List<SystemCar> selectByExampleAndPage(@Param("example") SystemCarExample example,@Param("page") Integer page);

    List<SystemCar> selectByExample(@Param("example") SystemCarExample example);

    SystemCar selectByPrimaryKey(Integer carId);

    SystemCar selectByDriverId(String driverId);

    int updateByExampleSelective(@Param("record") SystemCar record, @Param("example") SystemCarExample example);

    int updateByExample(@Param("record") SystemCar record, @Param("example") SystemCarExample example);

    int updateByPrimaryKeySelective(SystemCar record);

    int updateByPrimaryKey(SystemCar record);

    List<SystemCar> getAvaliableCar(@Param("beginTime") Long beginTime, @Param("endTime") Long endTime);

}