package com.carsystem.tools;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.carsystem.model.ShortMessage.MessageTemplate;

/**
 * 阿里短信工具
 *
 * @author xw
 */
public class MessageTool {
    /**
     * 产品名称:云通信短信API产品,开发者无需替换
     */
    private static final String PRODUCT = "Dysmsapi";
    /**
     * 产品域名,开发者无需替换
     */
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";
    /**
     * 阿里端accessKeyId
     */
    private static final String ACCESSKEY_ID = "LTAIAE3GAjNbrQtC";
    /**
     * 阿里端accessKeySecret
     */
    private static final String ACCESSKEY_SECRET = "zM3PlOEvBPf7jLWFyfy9E1PssoKPh9";

    //乘客预约审核成功信息发送给司机
    public static <T extends MessageTemplate> SendSmsResponse successMessageToDriver(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_133971800", "公务车预约系统");
    }

    //乘客预约审核成功信息发送给乘客
    public static <T extends MessageTemplate> SendSmsResponse successMessageToPassenger(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_133971801", "公务车预约系统");
    }

    //乘客预约审核失败信息发送给乘客
    public static <T extends MessageTemplate> SendSmsResponse FailMessageToPassenger(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_151771997", "公务车预约系统");
    }

    //乘客提交预约信息后向管理员发送提醒信息
    public static <T extends MessageTemplate> SendSmsResponse RemindMessageToAdmin(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_151767221", "公务车预约系统");
    }

    //司机提交请假信息后向管理员发送提醒信息
    public static <T extends MessageTemplate> SendSmsResponse RemindCarMessageToAdmin(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_151767489", "公务车预约系统");
    }

    //向司机发送请假申请审核通过
    public static <T extends MessageTemplate> SendSmsResponse SuccessCarMessageToAdmin(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_151772209", "公务车预约系统");
    }

    //向司机发送请假申请审核失败
    public static <T extends MessageTemplate> SendSmsResponse FailCarMessageToAdmin(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_151772210","公务车预约系统");
    }

    //发送验证码
    public static <T extends MessageTemplate> SendSmsResponse sendCode(T messageTemplate, String receivePhone) throws ClientException {
        return sendSms(messageTemplate, receivePhone, "SMS_133971797", "公务车预约系统");
    }

    public static <T extends MessageTemplate> SendSmsResponse sendSms(T messageTemplate, String receivePhone, String msgCode, String signName) throws ClientException {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile("cn-shanghai", ACCESSKEY_ID, ACCESSKEY_SECRET);
        DefaultProfile.addEndpoint("cn-shanghai", "cn-shanghai", PRODUCT, DOMAIN);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(receivePhone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(msgCode);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(JSON.toJSONString(messageTemplate));
        return acsClient.getAcsResponse(request);
    }

    public static String transferLongDateToString(Long time) {
        String strTime = time.toString();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(strTime.substring(0, 4));
        stringBuilder.append("年");
        stringBuilder.append(strTime.substring(4, 6));
        stringBuilder.append("月");
        stringBuilder.append(strTime.substring(6, 8));
        stringBuilder.append("日");
        stringBuilder.append(strTime.substring(8, 10));
        stringBuilder.append("时");
        stringBuilder.append(strTime.substring(10, 12));
        stringBuilder.append("分");
        return stringBuilder.toString();
    }

    public static String generateCode() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            int randNum = (int) (Math.random() * 10);
            stringBuilder.append(randNum);
        }
        return stringBuilder.toString();
    }
}
