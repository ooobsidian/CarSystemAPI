package com.carsystem.model.jsonrequestbody;

import com.alibaba.fastjson.annotation.JSONField;


public class RequestIdJson {
    @JSONField(name = "requestId")
    private Long requestId;

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }
}
