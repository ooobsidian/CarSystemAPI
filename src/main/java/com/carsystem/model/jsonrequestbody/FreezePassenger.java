package com.carsystem.model.jsonrequestbody;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @program: carsystem
 * @description: 冻结乘客
 * @author: xw
 * @create: 2018-11-26 19:55
 */
public class FreezePassenger {
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("passengerState")
    private Integer passengerState;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getPassengerState() {
        return passengerState;
    }

    public void setPassengerState(Integer passengerState) {
        this.passengerState = passengerState;
    }
}
