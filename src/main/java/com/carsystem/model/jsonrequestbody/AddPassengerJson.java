package com.carsystem.model.jsonrequestbody;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @program: carsystem
 * @description: 添加乘客信息前端提交的json
 * @author: xw
 * @create: 2018-11-25 21:16
 */
public class AddPassengerJson {

    @JSONField(name = "user_id")
    private String userId;
    @JSONField(name = "user_name")
    private String userName;
    @JSONField(name = "user_phone")
    private String userPhone;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
