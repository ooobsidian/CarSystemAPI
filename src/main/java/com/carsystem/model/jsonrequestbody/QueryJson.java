package com.carsystem.model.jsonrequestbody;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;


public class QueryJson {
    @JsonProperty("userId")
    private String userId;
    @JSONField(name = "type")
    private Integer type;
    @JSONField(name = "page")
    private Integer page;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}

