package com.carsystem.model.jsonrequestbody;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * @program: carsystem
 * @description: 预约公车
 * @author: xw
 * @create: 2018-11-26 21:07
 */
public class OrderCarJson {

    @JsonProperty("carId")
    private Integer carId;
    @JsonProperty("endDate")
    private String endDate;
    @JsonProperty("startDate")
    private String startDate;
    @JsonProperty("place")
    private String place;
    @JsonProperty("requestReason")
    private String requestReason;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getRequestReason() {
        return requestReason;
    }

    public void setRequestReason(String requestReason) {
        this.requestReason = requestReason;
    }
}
