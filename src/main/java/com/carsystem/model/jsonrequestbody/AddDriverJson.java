package com.carsystem.model.jsonrequestbody;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @program: carsystem
 * @description: 添加司机信息前端提交的json
 * @author: xw
 * @create: 2018-11-25 21:44
 */
public class AddDriverJson {
    @JSONField(name = "driver_id")
    private String driverId;
    @JSONField(name = "driver_name")
    private String driverName;
    @JSONField(name = "driver_phone")
    private String driverPhone;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }
}
