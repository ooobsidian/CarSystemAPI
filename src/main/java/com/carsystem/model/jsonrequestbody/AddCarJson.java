package com.carsystem.model.jsonrequestbody;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @program: carsystem
 * @description: 添加公车信息前端提交的json
 * @author: xw
 * @create: 2018-11-25 22:20
 */
public class AddCarJson {
    @JSONField(name = "carNumber")
    private String carNumber;
    @JSONField(name = "carModel")
    private String model;
    @JSONField(name = "carSize")
    private String carSize;
    @JSONField(name = "carRemark")
    private String carRemark;
    @JSONField(name = "driverId")
    private String driverId;
    @JSONField(name = "driverName")
    private String driveName;

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCarSize() {
        return carSize;
    }

    public void setCarSize(String carSize) {
        this.carSize = carSize;
    }

    public String getCarRemark() {
        return carRemark;
    }

    public void setCarRemark(String carRemark) {
        this.carRemark = carRemark;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDrivetName() {
        return driveName;
    }

    public void setDrivetName(String driveName) {
        this.driveName = driveName;
    }
}
