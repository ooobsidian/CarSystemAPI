package com.carsystem.model.jsonrequestbody;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @program: carsystem
 * @description: 冻结或解冻司机用户
 * @author: xw
 * @create: 2018-11-26 20:27
 */
public class FreezeDriver {
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("driverState")
    private Integer driverState;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getDriverState() {
        return driverState;
    }

    public void setDriverState(Integer driverState) {
        this.driverState = driverState;
    }
}
