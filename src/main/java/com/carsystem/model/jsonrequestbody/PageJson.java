package com.carsystem.model.jsonrequestbody;

import com.alibaba.fastjson.annotation.JSONField;


public class PageJson {
    @JSONField(name = "page")
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
