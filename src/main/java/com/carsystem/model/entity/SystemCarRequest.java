package com.carsystem.model.entity;

import java.util.Date;

public class SystemCarRequest {
    private Long carRequestId;

    private String driverId;

    private Integer carId;

    private String carNumber;

    private String carRequestReason;

    private Byte carRequestState;

    private Date carRequestTime;

    public Long getCarRequestId() {
        return carRequestId;
    }

    public void setCarRequestId(Long carRequestId) {
        this.carRequestId = carRequestId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId == null ? null : driverId.trim();
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber == null ? null : carNumber.trim();
    }

    public String getCarRequestReason() {
        return carRequestReason;
    }

    public void setCarRequestReason(String carRequestReason) {
        this.carRequestReason = carRequestReason == null ? null : carRequestReason.trim();
    }

    public Byte getCarRequestState() {
        return carRequestState;
    }

    public void setCarRequestState(Byte carRequestState) {
        this.carRequestState = carRequestState;
    }

    public Date getCarRequestTime() {
        return carRequestTime;
    }

    public void setCarRequestTime(Date carRequestTime) {
        this.carRequestTime = carRequestTime;
    }
}