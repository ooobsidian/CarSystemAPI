package com.carsystem.model.entity;

public class SystemAdmin {
    private String adminId;

    private String adminDuty;

    private Byte adminIdentity;

    private Byte adminState;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }

    public String getAdminDuty() {
        return adminDuty;
    }

    public void setAdminDuty(String adminDuty) {
        this.adminDuty = adminDuty == null ? null : adminDuty.trim();
    }

    public Byte getAdminIdentity() {
        return adminIdentity;
    }

    public void setAdminIdentity(Byte adminIdentity) {
        this.adminIdentity = adminIdentity;
    }

    public Byte getAdminState() {
        return adminState;
    }

    public void setAdminState(Byte adminState) {
        this.adminState = adminState;
    }
}