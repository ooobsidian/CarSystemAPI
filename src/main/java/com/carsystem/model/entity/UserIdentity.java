package com.carsystem.model.entity;

/**
 * 用户身份
 *
 * @author yoghurt
 */

public enum UserIdentity {
    /**
     * 乘客
     */
    PASSENGER(1, "乘客"),
    /**
     * 司机
     */
    DRIVER(2, "司机"),
    /**
     * 管理员
     */
    ADMIN(3, "管理员");

    private int value;
    private String description;

    UserIdentity(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
