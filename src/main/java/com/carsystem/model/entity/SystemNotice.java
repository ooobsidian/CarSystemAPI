package com.carsystem.model.entity;

import java.util.Date;

public class SystemNotice {
    private Integer noticeId;

    private String noticeContent;

    private String noticeIssuer;

    private Date createTime;

    private Date updateTime;

    public Integer getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    public String getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent == null ? null : noticeContent.trim();
    }

    public String getNoticeIssuer() {
        return noticeIssuer;
    }

    public void setNoticeIssuer(String noticeIssuer) {
        this.noticeIssuer = noticeIssuer == null ? null : noticeIssuer.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}