package com.carsystem.model.entity;

public class SystemDriver {
    private String driverId;

    private Byte driverState;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId == null ? null : driverId.trim();
    }

    public Byte getDriverState() {
        return driverState;
    }

    public void setDriverState(Byte driverState) {
        this.driverState = driverState;
    }
}