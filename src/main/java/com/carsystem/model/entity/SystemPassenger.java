package com.carsystem.model.entity;

public class SystemPassenger {
    private String passengerId;

    private Byte passengerPriority;

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId == null ? null : passengerId.trim();
    }

    public Byte getPassengerPriority() {
        return passengerPriority;
    }

    public void setPassengerPriority(Byte passengerPriority) {
        this.passengerPriority = passengerPriority;
    }
}