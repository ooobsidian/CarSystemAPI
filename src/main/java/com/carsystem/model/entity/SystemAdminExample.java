package com.carsystem.model.entity;

import java.util.ArrayList;
import java.util.List;

public class SystemAdminExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SystemAdminExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAdminIdIsNull() {
            addCriterion("admin_id is null");
            return (Criteria) this;
        }

        public Criteria andAdminIdIsNotNull() {
            addCriterion("admin_id is not null");
            return (Criteria) this;
        }

        public Criteria andAdminIdEqualTo(String value) {
            addCriterion("admin_id =", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotEqualTo(String value) {
            addCriterion("admin_id <>", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdGreaterThan(String value) {
            addCriterion("admin_id >", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdGreaterThanOrEqualTo(String value) {
            addCriterion("admin_id >=", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdLessThan(String value) {
            addCriterion("admin_id <", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdLessThanOrEqualTo(String value) {
            addCriterion("admin_id <=", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdLike(String value) {
            addCriterion("admin_id like", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotLike(String value) {
            addCriterion("admin_id not like", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdIn(List<String> values) {
            addCriterion("admin_id in", values, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotIn(List<String> values) {
            addCriterion("admin_id not in", values, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdBetween(String value1, String value2) {
            addCriterion("admin_id between", value1, value2, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotBetween(String value1, String value2) {
            addCriterion("admin_id not between", value1, value2, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminDutyIsNull() {
            addCriterion("admin_duty is null");
            return (Criteria) this;
        }

        public Criteria andAdminDutyIsNotNull() {
            addCriterion("admin_duty is not null");
            return (Criteria) this;
        }

        public Criteria andAdminDutyEqualTo(String value) {
            addCriterion("admin_duty =", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyNotEqualTo(String value) {
            addCriterion("admin_duty <>", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyGreaterThan(String value) {
            addCriterion("admin_duty >", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyGreaterThanOrEqualTo(String value) {
            addCriterion("admin_duty >=", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyLessThan(String value) {
            addCriterion("admin_duty <", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyLessThanOrEqualTo(String value) {
            addCriterion("admin_duty <=", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyLike(String value) {
            addCriterion("admin_duty like", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyNotLike(String value) {
            addCriterion("admin_duty not like", value, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyIn(List<String> values) {
            addCriterion("admin_duty in", values, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyNotIn(List<String> values) {
            addCriterion("admin_duty not in", values, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyBetween(String value1, String value2) {
            addCriterion("admin_duty between", value1, value2, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminDutyNotBetween(String value1, String value2) {
            addCriterion("admin_duty not between", value1, value2, "adminDuty");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityIsNull() {
            addCriterion("admin_identity is null");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityIsNotNull() {
            addCriterion("admin_identity is not null");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityEqualTo(Byte value) {
            addCriterion("admin_identity =", value, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityNotEqualTo(Byte value) {
            addCriterion("admin_identity <>", value, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityGreaterThan(Byte value) {
            addCriterion("admin_identity >", value, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityGreaterThanOrEqualTo(Byte value) {
            addCriterion("admin_identity >=", value, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityLessThan(Byte value) {
            addCriterion("admin_identity <", value, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityLessThanOrEqualTo(Byte value) {
            addCriterion("admin_identity <=", value, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityIn(List<Byte> values) {
            addCriterion("admin_identity in", values, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityNotIn(List<Byte> values) {
            addCriterion("admin_identity not in", values, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityBetween(Byte value1, Byte value2) {
            addCriterion("admin_identity between", value1, value2, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminIdentityNotBetween(Byte value1, Byte value2) {
            addCriterion("admin_identity not between", value1, value2, "adminIdentity");
            return (Criteria) this;
        }

        public Criteria andAdminStateIsNull() {
            addCriterion("admin_state is null");
            return (Criteria) this;
        }

        public Criteria andAdminStateIsNotNull() {
            addCriterion("admin_state is not null");
            return (Criteria) this;
        }

        public Criteria andAdminStateEqualTo(Byte value) {
            addCriterion("admin_state =", value, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateNotEqualTo(Byte value) {
            addCriterion("admin_state <>", value, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateGreaterThan(Byte value) {
            addCriterion("admin_state >", value, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateGreaterThanOrEqualTo(Byte value) {
            addCriterion("admin_state >=", value, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateLessThan(Byte value) {
            addCriterion("admin_state <", value, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateLessThanOrEqualTo(Byte value) {
            addCriterion("admin_state <=", value, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateIn(List<Byte> values) {
            addCriterion("admin_state in", values, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateNotIn(List<Byte> values) {
            addCriterion("admin_state not in", values, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateBetween(Byte value1, Byte value2) {
            addCriterion("admin_state between", value1, value2, "adminState");
            return (Criteria) this;
        }

        public Criteria andAdminStateNotBetween(Byte value1, Byte value2) {
            addCriterion("admin_state not between", value1, value2, "adminState");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}