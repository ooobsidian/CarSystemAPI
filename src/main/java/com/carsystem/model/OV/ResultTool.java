package com.carsystem.model.OV;


import static com.carsystem.model.OV.ResultCode.FAILED;
import static com.carsystem.model.OV.ResultCode.SUCCESS;

public class ResultTool {
    public static Result success(Object object){
        Result resultOV = new Result();
        resultOV.setCode(SUCCESS);
        resultOV.setData(object);
        return resultOV;
    }

    public static Result success(){
        Result resultOV = new Result();
        resultOV.setCode(SUCCESS);
        resultOV.setData(null);
        return resultOV;
    }

    public static Result error(){
        Result resultOV = new Result();
        resultOV.setCode(FAILED);
        resultOV.setData(null);
        return resultOV;
    }
    public static Result error(String error){
        Result resultOV = new Result();
        resultOV.setCode(FAILED);
        resultOV.setData(error);
        return resultOV;
    }
}
