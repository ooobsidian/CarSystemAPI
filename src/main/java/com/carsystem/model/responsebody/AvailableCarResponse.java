package com.carsystem.model.responsebody;
import com.carsystem.model.entity.SystemCar;

/**
 * @author xw
 */
public class AvailableCarResponse{
    private Integer carId;

    private String carNumber;

    private String driverName;

    private String carModel;

    private String carSize;

    private String carRemark;

    public AvailableCarResponse(SystemCar systemCar) {
        this.carId = systemCar.getCarId();
        this.carModel = systemCar.getCarModel();
        this.carNumber = systemCar.getCarNumber();
        this.carRemark = systemCar.getCarRemark();
        this.driverName = systemCar.getDriverName();
        this.carSize = systemCar.getCarSize();
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarSize() {
        return carSize;
    }

    public void setCarSize(String carSize) {
        this.carSize = carSize;
    }

    public String getCarRemark() {
        return carRemark;
    }

    public void setCarRemark(String carRemark) {
        this.carRemark = carRemark;
    }
}
