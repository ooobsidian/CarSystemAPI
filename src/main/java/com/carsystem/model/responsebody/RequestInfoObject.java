package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @program: carsystem
 * @description:
 * @author: xw
 * @create: 2018-11-27 23:29
 */
public class RequestInfoObject {
    @JsonProperty("total")
    private int total;
    @JsonProperty("requestInfo")
    private List requestInfo;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List getRequestInfo() {
        return requestInfo;
    }

    public void setRequestInfo(List requestInfo) {
        this.requestInfo = requestInfo;
    }
}
