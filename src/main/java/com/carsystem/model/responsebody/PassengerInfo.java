package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PassengerInfo {
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("usePhone")
    private String userPhone;
    @JsonProperty("useImg")
    private String userImg;
    @JsonProperty("department")
    private String department;
    @JsonProperty("duty")
    private String duty;
    @JsonProperty("userState")
    private String passengerState;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getPassengerState() {
        return passengerState;
    }

    public void setPassengerState(String passengerState) {
        this.passengerState = passengerState;
    }
}
