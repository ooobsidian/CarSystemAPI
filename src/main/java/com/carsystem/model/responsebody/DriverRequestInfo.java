package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;


public class DriverRequestInfo {

    @JsonProperty("startDate")
    private String beginTime;
    @JsonProperty("endDate")
    private String endTime;
    @JsonProperty("pass")
    private byte requestState;
    @JsonProperty("reason")
    private String requestReason;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("id")
    private Long requestId;
    @JsonProperty("avatar")
    private String userImg;
    @JsonProperty("owner")
    private String driverName;
    @JsonProperty("licence")
    private String carNumber;
    @JsonProperty("model")
    private String carModel;
    @JsonProperty("capacity")
    private String carSize;
    @JsonProperty("cid")
    private Integer carId;
    @JsonProperty("passengerPhone")
    private String passengerPhone;
    @JsonProperty("place")
    private String place;

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public byte getRequestState() {
        return requestState;
    }

    public void setRequestState(byte requestState) {
        this.requestState = requestState;
    }

    public String getRequestReason() {
        return requestReason;
    }

    public void setRequestReason(String requestReason) {
        this.requestReason = requestReason;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarSize() {
        return carSize;
    }

    public void setCarSize(String carSize) {
        this.carSize = carSize;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }


    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPassengerPhone() {
        return passengerPhone;
    }

    public void setPassengerPhone(String passengerPhone) {
        this.passengerPhone = passengerPhone;
    }
}
