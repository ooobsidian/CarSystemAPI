package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @program: carsystem
 * @description:
 * @author: xw
 * @create: 2018-11-29 01:09
 */
public class CarInfo {
    @JsonProperty("carId")
    private Integer carId;
    @JsonProperty("carNumber")
    private String carNumber;
    @JsonProperty("driverId")
    private String driverId;
    @JsonProperty("driverName")
    private String driverName;
    @JsonProperty("userName")
    private String carModel;
    @JsonProperty("carSize")
    private String carSize;
    @JsonProperty("carRemark")
    private String carRemark;
    @JsonProperty("userState")
    private String carState;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarSize() {
        return carSize;
    }

    public void setCarSize(String carSize) {
        this.carSize = carSize;
    }

    public String getCarRemark() {
        return carRemark;
    }

    public void setCarRemark(String carRemark) {
        this.carRemark = carRemark;
    }

    public String getCarState() {
        return carState;
    }

    public void setCarState(String carState) {
        this.carState = carState;
    }
}
