package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.Date;
public class CarRequestInfo {
    @JsonProperty("status")
    private Byte carRequestState;
    @JsonProperty("owner")
    private String driverName;
    @JsonProperty("phoneno")
    private String driverPhone;
    @JsonProperty("time")
    private String carRequestTime;
    @JsonProperty("licence")
    private String carNumber;
    @JsonProperty("reason")
    private String reason;

    public Byte getCarRequestState() {
        return carRequestState;
    }

    public void setCarRequestState(Byte carRequestState) {
        this.carRequestState = carRequestState;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getCarRequestTime() {
        return carRequestTime;
    }

    public void setCarRequestTime(String carRequestTime) {
        this.carRequestTime = carRequestTime;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
