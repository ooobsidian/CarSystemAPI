package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DriverInfo {
    @JsonProperty("driverId")
    private String userId;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("driverPhone")
    private String userPhone;
    @JsonProperty("driverImg")
    private String userImg;
    @JsonProperty("department")
    private String department;
    @JsonProperty("duty")
    private String duty;
    @JsonProperty("carId")
    private Integer carId;
    @JsonProperty("carNumber")
    private String carNumber;
    @JsonProperty("carModel")
    private String carModel;
    @JsonProperty("carSize")
    private String carSize;
    @JsonProperty("carState")
    private String carState;
    @JsonProperty("userState")
    private String driverState;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarSize() {
        return carSize;
    }

    public void setCarSize(String carSize) {
        this.carSize = carSize;
    }

    public String getCarState() {
        return carState;
    }

    public void setCarState(String carState) {
        this.carState = carState;
    }

    public String getDriverState() {
        return driverState;
    }

    public void setDriverState(String driverState) {
        this.driverState = driverState;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }
}
