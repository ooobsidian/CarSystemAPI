package com.carsystem.model.responsebody;

import com.carsystem.model.entity.SystemRequest;

import java.util.List;

/**
 * @program: carsystem
 * @description:
 * @author: xw
 * @create: 2018-11-28 00:17
 */
public class RequestObject {

    private int total;
    private List<SystemRequest> requestInfo;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SystemRequest> getRequestInfo() {
        return requestInfo;
    }

    public void setRequestInfo(List<SystemRequest> requestInfo) {
        this.requestInfo = requestInfo;
    }
}
