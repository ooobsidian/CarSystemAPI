package com.carsystem.model.responsebody;

import com.fasterxml.jackson.annotation.JsonProperty;


public class InsertCarRequest {
    @JsonProperty("status")
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
