package com.carsystem.model.ShortMessage;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @program: carsystem
 * @description: 司机提交请假申请后向管理员发送提醒信息
 * @author: xw
 * @create: 2018-11-25 00:32
 */
public class CarRequestRemind extends MessageTemplate{
    @JSONField(name = "driver_name")
    private String driverName;
    @JSONField(name = "driver_id")
    private String driverId;
    @JSONField(name = "car_id")
    private Integer carId;
    @JSONField(name = "time")
    private String date;

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
