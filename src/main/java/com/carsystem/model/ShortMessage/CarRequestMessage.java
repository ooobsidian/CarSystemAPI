package com.carsystem.model.ShortMessage;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @program: carsystem
 * @description:
 * @author: xw
 * @create: 2018-11-25 00:51
 */
public class CarRequestMessage extends MessageTemplate{
    @JSONField(name = "driver_name")
    private String driverName;
    @JSONField(name = "time")
    private String date;
    @JSONField(name = "car_id")
    private Integer carId;

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }
}
