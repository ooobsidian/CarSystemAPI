package com.carsystem.model.ShortMessage;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;


/**
 * @program: carsystem
 * @description: 乘客提交预约后发短信提醒管理员
 * @author: xw
 * @create: 2018-11-25 00:20
 */
public class RequestRemind extends MessageTemplate {
    @JSONField(name = "passenger_id")
    private String passengerId;
    @JSONField(name = "request_time")
    private String requestTime;

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }
}
