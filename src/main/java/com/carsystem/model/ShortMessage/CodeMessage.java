package com.carsystem.model.ShortMessage;

/**
 * 验证码模板
 *
 * @author xw
 */
public class CodeMessage extends MessageTemplate {
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
