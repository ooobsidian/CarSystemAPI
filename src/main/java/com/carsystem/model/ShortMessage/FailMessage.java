package com.carsystem.model.ShortMessage;

import com.alibaba.fastjson.annotation.JSONField;
import com.carsystem.tools.MessageTool;


/**
 * @program: carsystem
 * @description: 预约失败后发送的短信模板
 * @author: xw
 * @create: 2018-11-24 22:56
 */
public class FailMessage extends MessageTemplate {
    @JSONField(name = "passenger_name")
    private String passengerName;
    @JSONField(name = "begin_time")
    private String beginTime;
    @JSONField(name = "end_time")
    private String endTime;
    @JSONField(name = "passenger_phone")
    private String passengerPhone;

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPassengerPhone() {
        return passengerPhone;
    }

    public void setPassengerPhone(String passengerPhone) {
        this.passengerPhone = passengerPhone;
    }
}
