package com.carsystem.model.ShortMessage;

import com.alibaba.fastjson.annotation.JSONField;


/**
 * 预约成功后发送的短信模板
 *
 * @author xw
 */
public class SuccessMessage extends MessageTemplate {
    @JSONField(name = "passenger_name")
    private String passengerName;
    @JSONField(name = "begin_time")
    private String beginTime;
    @JSONField(name = "end_time")
    private String endTime;
    private String place;
    @JSONField(name = "driver_name")
    private String driverName;
    @JSONField(name = "driver_phone")
    private String driverPhone;
    @JSONField(name = "passenger_phone")
    private String passengerPhone;

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getPassengerPhone() {
        return passengerPhone;
    }

    public void setPassengerPhone(String passengerPhone) {
        this.passengerPhone = passengerPhone;
    }
}
