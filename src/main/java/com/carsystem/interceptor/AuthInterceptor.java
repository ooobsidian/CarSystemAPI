
package com.carsystem.interceptor;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.carsystem.tools.JwtUtil;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器配置
 * @author xw
 */
public class AuthInterceptor implements HandlerInterceptor {
    private static final String LOGIN_URL = "/login";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // 登陆接口不做拦截
        if (LOGIN_URL.equals(request.getRequestURI())) {
            return true;
        }
        try {
            String userId = JwtUtil.parseJwt(request.getHeader("Authorization"));
            return userId != null;
        } catch (JWTVerificationException e) {
            return false;
        }

    }
}
