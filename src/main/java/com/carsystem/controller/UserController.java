package com.carsystem.controller;


import com.carsystem.dao.SystemUserMapper;
import com.carsystem.model.OV.Result;
import com.carsystem.model.OV.ResultTool;
import com.carsystem.model.ShortMessage.ValidateCode;
import com.carsystem.model.entity.SystemUser;
import com.carsystem.model.jsonrequestbody.NewPhoneNum;
import com.carsystem.service.UserService;
import com.carsystem.tools.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xw
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api/user")
@Api(description = "用户通用接口")
public class UserController {

    @Resource
    private final UserService userService;
    @Resource
    private SystemUserMapper systemUserMapper;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/validateCode", method = RequestMethod.GET)
    @ApiOperation(value = "给旧手机号发送验证码", httpMethod = "GET")
    public Result<ValidateCode> codeToOldPhone(@RequestHeader(value = "Authorization") String token) {
        String userId = JwtUtil.parseJwt(token);
        return userService.sendCodeToExistPhone(userId);
    }

    @RequestMapping(value = "/checkValidateCode", method = RequestMethod.POST)
    @ApiOperation(value = "验证验证码", httpMethod = "POST")
    public Result checkValidateCode(@RequestBody ValidateCode validateCode) {
        return userService.verifyCode(validateCode);
    }

    @RequestMapping(value = "/validateCode", method = RequestMethod.POST)
    @ApiOperation(value = "给新手机号发送验证码", httpMethod = "POST")
    public Result<ValidateCode> codeToNewPhone(@RequestBody NewPhoneNum newPhoneNum) {
        return userService.sendCodeToNewPhone(newPhoneNum.getPhoneNum());
    }

    @RequestMapping(value = "/checkValidateCodeForNewPhone", method = RequestMethod.POST)
    @ApiOperation(value = "验证修改手机号验证码", httpMethod = "POST")
    public Result codeToNewPhone(@RequestHeader(value = "Authorization") String token, @RequestBody ValidateCode validateCode) {
        String userId = JwtUtil.parseJwt(token);
        return userService.verifyCodeAndUpdatePhone(userId, validateCode);
    }

    @RequestMapping(value = "/phoneNum",method = RequestMethod.GET)
    @ApiOperation(value = "查询用户手机号", httpMethod = "GET")
    public Result queryPhoneNum(@RequestHeader(value = "Authorization") String token){
        String userId = JwtUtil.parseJwt(token);
        Result result = new Result();
        try{
            SystemUser systemUser = systemUserMapper.selectByPrimaryKey(userId);
            String phoneNum = systemUser.getUserPhone();
            result = ResultTool.success(phoneNum);
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询失败");
        }

        return result;

    }

}
