

package com.carsystem.controller;

import com.carsystem.model.LoginResponse.LoginUser;
import com.carsystem.model.OV.Result;
import com.carsystem.model.LoginResponse.TokenResponse;
import com.carsystem.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author xw
 */
@RestController
@RequestMapping(value = "/login")
@CrossOrigin
@Api(description = "登陆")
public class LoginController {
    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "登陆", httpMethod = "POST")
    public Result<TokenResponse> login(@RequestBody LoginUser loginUser) {
        return userService.login(loginUser);
    }

}
