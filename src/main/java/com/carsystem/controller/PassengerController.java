package com.carsystem.controller;

import com.carsystem.model.jsonrequestbody.OrderCarJson;
import com.carsystem.model.jsonrequestbody.RequestIdJson;
import com.carsystem.model.responsebody.PassengerInfo;
import com.carsystem.model.responsebody.RequestInfo;
import com.carsystem.model.OV.Result;
import com.carsystem.model.jsonrequestbody.QueryJson;
import com.carsystem.model.OV.ResultCode;
import com.carsystem.model.responsebody.AvailableCarResponse;
import com.carsystem.service.PassengerService;
import com.carsystem.tools.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * @author xw
 */
@RestController
@CrossOrigin
@RequestMapping("/api/passenger")
@Api(description = "乘客接口")
public class PassengerController {
    @Resource
    private PassengerService passengerService;
    private static final long ORDER_TIME_LIMIT = 100;

    @RequestMapping(value = "/availableCar", method = RequestMethod.GET)
    @ApiOperation(value = "获取可预约的车辆信息", httpMethod = "GET")
    public Result<List<AvailableCarResponse>> getCarList(@RequestParam String startDate,
                                                         @RequestParam String endDate,
                                                         @RequestHeader(value = "Authorization") String token)throws ParseException {
        Result<List<AvailableCarResponse>> result = new Result<>();
        // 日期数据格式化为Long型数据，例：201802230118
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        Long startTime = Long.parseLong(sdf.format(sdf.parse(startDate)));
        Long endTime = Long.parseLong(sdf.format(sdf.parse(endDate)));
        if (endTime - startTime <= ORDER_TIME_LIMIT) {
            result.setCode(ResultCode.FAILED);
            result.setMessage("预约起始时间需超过预约结束时间一个小时");
            return result;
        }
        return passengerService.getAvailableCar(startTime, endTime);
    }

    @RequestMapping(value = "/orderCar",method = RequestMethod.POST)
    @ApiOperation(value = "提交预约申请",httpMethod = "POST")
    public Result orderCar(@RequestHeader(value = "Authorization") String token,
                           @RequestBody OrderCarJson orderCarJson) throws ParseException{
        String userId = JwtUtil.parseJwt(token);
        return  passengerService.OrderCar(userId,orderCarJson);

    }


    @RequestMapping(value = "/query/userInfo", method = RequestMethod.GET)
    @ApiOperation(value = "乘客查询个人信息", httpMethod = "GET")
    public Result queryPassengerInfo(@RequestHeader(value = "Authorization") String token) {
        String userId = JwtUtil.parseJwt(token);
        return passengerService.queryPassengerInfo(userId);
    }

    @RequestMapping(value = "/query/request", method = RequestMethod.POST)
    @ApiOperation(value = "用户查询预约请求", httpMethod = "POST")
    public Result QueryRequestList(@RequestBody QueryJson queryJson,
                                                      @RequestHeader(value = "Authorization") String token) {
        String userId = JwtUtil.parseJwt(token);
        return passengerService.queryRequestInfo(userId, queryJson);
    }

    @RequestMapping(value = "/cancel/request", method = RequestMethod.POST)
    @ApiOperation(value = "取消正在申请预约", httpMethod = "POST")
    public Result CancelRequest(@RequestBody RequestIdJson requestIdJson,
                                @RequestHeader(value = "Authorization") String token) {
        String userId = JwtUtil.parseJwt(token);
        return passengerService.deleteRequest(userId, requestIdJson);
    }



}
