package com.carsystem.controller;

import com.carsystem.model.OV.*;
import com.carsystem.model.jsonrequestbody.PageJson;
import com.carsystem.model.jsonrequestbody.QueryJson;
import com.carsystem.service.DriverService;
import com.carsystem.tools.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xw
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api/driver")
@Api(description = "司机端接口")
public class DriverController {

    @Resource
    private DriverService driverService;

    @RequestMapping(value = "/query/driverInfo", method = RequestMethod.GET)
    @ApiOperation(value = "查询司机个人信息", httpMethod = "GET")
    public Result QueryDriverInfo(@RequestHeader(value = "Authorization") String token) {
        String userId = JwtUtil.parseJwt(token);
        return driverService.queryDriverInfo(userId);
    }


    @RequestMapping(value = "/query/request", method = RequestMethod.POST)
    @ApiOperation(value = "查询预约请求", httpMethod = "POST")
    public Result QueryRequestList(@RequestBody QueryJson queryJson,
                                   @RequestHeader(value = "Authorization") String token) {
        String userId = JwtUtil.parseJwt(token);
        return driverService.driverQueryRequestInfo(userId, queryJson);
    }

    @RequestMapping(value = "/insert/carRequest", method = RequestMethod.POST)
    @ApiOperation(value = "提交车辆保修申请", httpMethod = "POST")
    public Result InsertCarRequestList(@RequestHeader(value = "Authorization") String token,
                                       @RequestParam String reason) {
        String userId = JwtUtil.parseJwt(token);
        return driverService.insertCarRequest(userId, reason);
    }

    @RequestMapping(value = "/query/carRequest", method = RequestMethod.POST)
    @ApiOperation(value = "查询车辆保修申请信息", httpMethod = "POST")
    public Result QueryCarRequestList(@RequestHeader(value = "Authorization") String token,
                                      @RequestBody PageJson pageJson) {
        String userId = JwtUtil.parseJwt(token);
        return driverService.queryCarRequestInfo(userId, pageJson);
    }



}
