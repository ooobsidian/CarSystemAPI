package com.carsystem.controller;

import com.aliyuncs.exceptions.ClientException;
import com.carsystem.model.OV.Result;
import com.carsystem.model.jsonrequestbody.*;
import com.carsystem.service.AdminService;
import com.carsystem.service.DriverService;
import com.carsystem.service.PassengerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * @program: carsystem
 * @description: 管理员端
 * @author: xw
 * @create: 2018-11-24 14:23
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/api/admin")
@Api(description = "管理员端接口")
public class AdminController {

    @Resource
    private AdminService adminService;
    @Resource
    private PassengerService passengerService;

    @Resource
    private DriverService driverService;

    @RequestMapping(value = "/query/requestInfo/passenger", method = RequestMethod.POST)
    @ApiOperation(value = "查询乘客预约请求", httpMethod = "POST")
    public Result queryPassengerRequestInfo(@RequestBody QueryJson queryJson){
        return passengerService.queryRequestInfo(queryJson.getUserId(),queryJson);
    }

    @RequestMapping(value = "/query/requestInfo/driver",method = RequestMethod.POST)
    @ApiOperation(value = "查询司机预约请求",httpMethod = "POST")
    public Result queryDriverRequestInfo(@RequestBody QueryJson queryJson){
        return driverService.driverQueryRequestInfo(queryJson.getUserId(),queryJson);
    }

    @RequestMapping(value = "/query/car/request",method = RequestMethod.GET)
    @ApiOperation(value = "查询车辆的所有预约请求",httpMethod = "GET")
    public Result queryRequest(@RequestParam Integer carId){
        return adminService.getRequestInfo(carId);
    }

    @RequestMapping(value = "/check/request",method = RequestMethod.POST)
    @ApiOperation(value = "审核乘客提交的用车申请",httpMethod = "POST")
    public Result checkRequest (@RequestParam Long requestId,
                               @RequestParam Integer requestState) throws ClientException {
        return adminService.checkRequest(requestId,requestState);
    }

    @RequestMapping(value = "/check/carRequest", method = RequestMethod.POST)
    @ApiOperation(value = "审核司机提交的请假申请",httpMethod = "POST")
    public Result checkCarRequest (@RequestParam Long carRequestId,
                                   @RequestParam Integer requestState) throws ClientException {
        return adminService.checkCarRequest(carRequestId,requestState);
    }

    @RequestMapping(value = "/add/passenger",method = RequestMethod.POST)
    @ApiOperation(value = "添加乘客用户",httpMethod = "POST")
    public Result addPassengerInfo(@RequestBody AddPassengerJson addPassengerJson){
        return adminService.AddPassengerInfo(addPassengerJson);
    }

    @RequestMapping(value = "/add/driver", method = RequestMethod.POST)
    @ApiOperation(value = "添加司机用户",httpMethod = "POST")
    public Result addDriverInfo(@RequestBody AddDriverJson addDriverJson){
        return adminService.AddDriverInfo(addDriverJson);
    }

    @RequestMapping(value = "/add/carInfo",method = RequestMethod.POST)
    @ApiOperation(value = "添加公车信息", httpMethod = "POST")
    public Result addCarInfo(@RequestBody AddCarJson addCarJson){
        return adminService.AddCarInfo(addCarJson);
    }

    @RequestMapping(value = "/query/passage",method = RequestMethod.POST)
    @ApiOperation(value = "查询乘客信息",httpMethod = "POST")
    public Result queryPassageInfo(@RequestBody PageJson pageJson){
        return adminService.queryPassenger(pageJson);
    }

    @RequestMapping(value = "/query/driver",method = RequestMethod.POST)
    @ApiOperation(value = "查询司机信息", httpMethod = "POST")
    public Result queryDriverInfo(@RequestBody PageJson pageJson){
        return adminService.queryDriver(pageJson);
    }

    @RequestMapping(value = "/query/car",method = RequestMethod.POST)
    @ApiOperation(value = "查询公车信息",httpMethod = "POST")
    public Result queryCarInfo(@RequestBody PageJson pageJson){
        return adminService.queryCarInfo(pageJson);
    }

    @RequestMapping(value = "/changeState/passenger",method = RequestMethod.POST)
    @ApiOperation(value = "修改乘客状态",httpMethod = "POST")
    public Result changeStatePassenger(@RequestBody FreezePassenger freezePassenger){
        return adminService.FreezePassenger(freezePassenger);
    }

    @RequestMapping(value = "/changeState/driver",method = RequestMethod.POST)
    @ApiOperation(value = "修改司机状态",httpMethod = "POST")
    public Result changeStateDriver(@RequestBody FreezeDriver freezeDriver){
        return adminService.FreezeDriver(freezeDriver);
    }


}
