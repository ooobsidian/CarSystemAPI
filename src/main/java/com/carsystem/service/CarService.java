package com.carsystem.service;

import com.carsystem.dao.SystemCarMapper;
import com.carsystem.model.entity.SystemCar;
import com.carsystem.model.entity.SystemCarExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    @Resource
    private SystemCarMapper systemCarMapper;

    /**
     * 查询汽车信息
     * @param carId
     * @return
     */
    public SystemCar queryCarInfo(Integer carId){
        SystemCar systemCar = new SystemCar();
        systemCar = systemCarMapper.selectByPrimaryKey(carId);
        return systemCar;
    }

    public SystemCar queryByDriverId(String DriverId){
        List<SystemCar> systemCars = new ArrayList<>();
        SystemCarExample example = new SystemCarExample();
        example.createCriteria()
                .andDriverIdEqualTo(DriverId);
        systemCars = systemCarMapper.selectByExample(example);
        SystemCar systemCar = new SystemCar();
        for(SystemCar car:systemCars){
            systemCar = car;
        }
        return systemCar;
    }
}
