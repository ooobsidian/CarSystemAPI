package com.carsystem.service;

import com.carsystem.dao.SystemCarMapper;
import com.carsystem.dao.SystemCarRequestMapper;
import com.carsystem.dao.SystemRequestMapper;
import com.carsystem.dao.SystemUserMapper;
import com.carsystem.model.OV.*;
import com.carsystem.model.ShortMessage.CarRequestRemind;
import com.carsystem.model.jsonrequestbody.PageJson;
import com.carsystem.model.entity.*;
import com.carsystem.model.jsonrequestbody.QueryJson;
import com.carsystem.model.responsebody.*;
import com.carsystem.tools.MessageTool;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DriverService {
    @Resource
    private SystemRequestMapper systemRequestMapper;
    @Resource
    private SystemUserMapper systemUserMapper;
    @Resource
    private SystemCarMapper systemCarMapper;
    @Resource
    private SystemCarRequestMapper systemCarRequestMapper;
    @Resource
    private UserService userService;
    @Resource
    private CarService carService;
    @Resource
    private DriverService driverService;

    /**
     * 司机用户查询个人信息
     *
     * @param userId
     * @return 返回封装好的Result对象
     */
    public Result queryDriverInfo(String userId) {
        Result<DriverInfo> result = new Result<>();
        try {
            //1.查询符合条件的数据
            SystemUser systemUser = userService.queryUserInfo(userId);
            SystemCar systemCar = carService.queryByDriverId(userId);
            //2.数据拼接
            DriverInfo driverInfo = new DriverInfo();
            driverInfo.setUserId(systemUser.getUserId());
            driverInfo.setUserName(systemUser.getUserName());
            driverInfo.setUserPhone(systemUser.getUserPhone());
            driverInfo.setUserImg(systemUser.getUserImg());
            driverInfo.setDepartment(systemUser.getDepartment());
            driverInfo.setDuty(systemUser.getDuty());
            driverInfo.setCarNumber(systemCar.getCarNumber());
            driverInfo.setCarModel(systemCar.getCarModel());
            driverInfo.setCarSize(systemCar.getCarSize());
            if (driverInfo != null) {
                result = ResultTool.success(driverInfo);
            } else {
                result = ResultTool.error();
                result.setMessage("查询不到用户信息！");
            }
            return result;
        } catch (Exception e) {
            result = ResultTool.error();
            result.setMessage("查询不失败！");
            return result;
        }
    }

    /**
     * 查询预约请求
     *
     * @param userId
     * @param page
     * @return 返回符合条件的预约请求列表
     */
    public RequestObject driverQueryRequest(String userId, Integer type, Integer page) {
        Byte success = 3;
        RequestObject requestObject = new RequestObject();
        int count = 0;
        //获取当前的日期
        Date date = new Date();
        //设置时间格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        //将时间，类型转化为String时间
        String createdate = sdf.format(date);
        Long time = Long.parseLong(createdate);
        List<SystemRequest> systemRequests = new ArrayList<>();
        try {
            if(type == 0){
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andDriverIdEqualTo(userId)
                        .andRequestStateEqualTo(success)
                        .andBeginTimeGreaterThanOrEqualTo(time);
                SystemRequestExample example1 = new SystemRequestExample();
                example1.createCriteria()
                        .andDriverIdLike(userId)
                        .andRequestStateEqualTo(success)
                        .andBeginTimeGreaterThanOrEqualTo(time);
                count = systemRequestMapper.countByExample(example1);
                systemRequests =  systemRequestMapper.selectByExample((page-1)*10,example);
            }else if(type == 1){
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andDriverIdEqualTo(userId)
                        .andRequestStateEqualTo(success)
                        .andBeginTimeLessThan(time)
                        .andEndTimeGreaterThan(time);
                SystemRequestExample example1 = new SystemRequestExample();
                example1.createCriteria()
                        .andDriverIdLike(userId)
                        .andRequestStateEqualTo(success)
                        .andBeginTimeLessThan(time)
                        .andEndTimeGreaterThan(time);
                count = systemRequestMapper.countByExample(example1);
                systemRequests =  systemRequestMapper.selectByExample((page-1)*10,example);
            }else if(type==2){
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andDriverIdEqualTo(userId)
                        .andRequestStateEqualTo(success)
                        .andEndTimeLessThan(time);
                SystemRequestExample example1 = new SystemRequestExample();
                example1.createCriteria()
                        .andDriverIdLike(userId)
                        .andRequestStateEqualTo(success)
                        .andEndTimeLessThan(time);
                count = systemRequestMapper.countByExample(example1);
                systemRequests =  systemRequestMapper.selectByExample((page-1)*10,example);
            } else{
                systemRequests = null;
            }

            requestObject.setTotal(count);
            requestObject.setRequestInfo(systemRequests);

            return requestObject;

        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查询预约请求
     *
     * @param userId
     * @param
     * @return 返回封装好后的Result对象
     */
    public Result driverQueryRequestInfo(String userId, QueryJson queryJson) {
        Integer page = queryJson.getPage();
        Integer type = queryJson.getType();
        Result<List<DriverRequestInfo>> result = new Result<>();
        //1.查询符合条件的数据
        RequestObject requestObject = driverService.driverQueryRequest(userId,type,page);
        List<SystemRequest> requestList = requestObject.getRequestInfo();
        //2.数据拼接
        List<DriverRequestInfo> requestInfoList = new ArrayList<>();
        try{
            if(requestList != null){
                for (SystemRequest request : requestList) {
                    //用于获取汽车相关信息
                    SystemCar systemCar = carService.queryCarInfo(request.getCarId());
                    //用于获取司机用户的电话
                    SystemUser systemUser1 = systemUserMapper.selectByPrimaryKey(request.getDriverId());
                    //用与获取乘客的相关信息
                    SystemUser systemUser = systemUserMapper.selectByPrimaryKey(request.getPassengerId());
                    DriverRequestInfo requestInfo = new DriverRequestInfo();
                    requestInfo.setBeginTime(MessageTool.transferLongDateToString(request.getBeginTime()));
                    requestInfo.setEndTime(MessageTool.transferLongDateToString(request.getEndTime()));
                    requestInfo.setRequestState(request.getRequestState());
                    requestInfo.setRequestReason(request.getRequestReason());
                    requestInfo.setUserId(request.getPassengerId());
                    requestInfo.setUserName(systemUser.getUserName());
                    requestInfo.setRequestId(request.getRequestId());
                    requestInfo.setUserImg(systemUser.getUserImg());
                    requestInfo.setDriverName(systemCar.getDriverName());
                    requestInfo.setCarNumber(systemCar.getCarNumber());
                    requestInfo.setCarModel(systemCar.getCarModel());
                    requestInfo.setCarSize(systemCar.getCarSize());
                    requestInfo.setCarId(systemCar.getCarId());
                    requestInfo.setPassengerPhone(systemUser.getUserPhone());
                    requestInfo.setPlace(request.getPlace());
                    requestInfoList.add(requestInfo);
                }
                RequestInfoObject requestInfoObject = new RequestInfoObject();
                requestInfoObject.setTotal(requestObject.getTotal());
                requestInfoObject.setRequestInfo(requestInfoList);
                result = ResultTool.success(requestInfoObject);
                return result;
            }else{
                result = ResultTool.error();
                result.setMessage("未查询到结果！");
                return result;
            }
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询失败！");
            return result;
        }
}

    /**
     * 车辆保修
     *
     * @param userId
     * @param reason
     * @return 返回封装后的结果
     */
    public Result insertCarRequest(String userId, String reason) {
        String driverName = null ;
        //获取当前的日期
        Date date = new Date();
        //设置时间格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        //将时间，类型转化为String时间
        String createdate = sdf.format(date);
        Long time = Long.parseLong(createdate);
        SystemCarExample example = new SystemCarExample();
        example.createCriteria()
                .andDriverIdEqualTo(userId);
        List<SystemCar> carInfoList = systemCarMapper.selectByExample(example);
        SystemCarRequest systemCarRequest = new SystemCarRequest();
        for (SystemCar carInfo : carInfoList) {
            systemCarRequest.setCarNumber(carInfo.getCarNumber());
            systemCarRequest.setCarId(carInfo.getCarId());
            systemCarRequest.setCarRequestReason(reason);
            systemCarRequest.setCarRequestState((byte) 1);
            systemCarRequest.setDriverId(carInfo.getDriverId());
            driverName= carInfo.getDriverName();
        }
        int res = systemCarRequestMapper.insert(systemCarRequest);
        Result result = new Result();
        if (res > 0) {
            result = ResultTool.success();
        } else {
            result = ResultTool.error();
        }
        //查找出所有管理员
        SystemUserExample example1 = new SystemUserExample();
        example1.createCriteria()
                .andUserIdentityEqualTo((byte) 3);
        List<SystemUser> adminList = systemUserMapper.selectByExample(example1);
        for (SystemUser admin : adminList) {
            try{
                CarRequestRemind carRequestRemind = new CarRequestRemind();
                carRequestRemind.setDriverId(userId);
                carRequestRemind.setDriverName(driverName);
                carRequestRemind.setDate(MessageTool.transferLongDateToString(time));
                carRequestRemind.setCarId(1);
                MessageTool.RemindCarMessageToAdmin(carRequestRemind, admin.getUserPhone());
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return result;
    }

    /**
     * 查询保修请求
     *
     * @param userId
     * @param page
     * @return 返回满足查询条件的预约申请列表
     */
    public List<SystemCarRequest> queryCarRequest(String userId, Integer page) {

        try {
            SystemCarRequestExample example = new SystemCarRequestExample();
            example.createCriteria()
                    .andDriverIdEqualTo(userId);
            List<SystemCarRequest> carRequests = systemCarRequestMapper.selectByExample(example,(page-1)*10);
            return carRequests;
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * 司机用户查询保修申请
     *
     * @param userId
     * @param
     * @return 返回封装好的Result对象
     */
    public Result queryCarRequestInfo(String userId, PageJson pageJson) {
        RequestInfoObject requestInfoObject = new RequestInfoObject();
        SystemCarRequestExample example = new SystemCarRequestExample();
        example.createCriteria()
                .andDriverIdEqualTo(userId);
        int count = systemCarRequestMapper.countByExample(example);
        //1.获取符合条件的数据
        List<SystemCarRequest> systemCarRequestsList = driverService.queryCarRequest(userId, pageJson.getPage());
        //2.数据拼接
        List<CarRequestInfo> carRequestInfoList = new ArrayList<>();
        Result result = new Result<>();
        try {
            for (SystemCarRequest request : systemCarRequestsList) {
                //用于获取汽车相关信息
                SystemCar systemCar = carService.queryCarInfo(request.getCarId());
                //用于获取司机用户的电话
                SystemUser systemUser = systemUserMapper.selectByPrimaryKey(request.getDriverId());
                CarRequestInfo carRequestInfo = new CarRequestInfo();
                carRequestInfo.setCarRequestState(request.getCarRequestState());
                carRequestInfo.setDriverName(systemUser.getUserName());
                carRequestInfo.setDriverPhone(systemUser.getUserPhone());
                //设置时间格式
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
                //将时间，类型转化为String时间
                String createdate = sdf.format(request.getCarRequestTime());
                Long time = Long.parseLong(createdate);
                carRequestInfo.setCarRequestTime(MessageTool.transferLongDateToString(time));
                carRequestInfo.setCarNumber(request.getCarNumber());
                carRequestInfo.setReason(request.getCarRequestReason());
                carRequestInfoList.add(carRequestInfo);
            }
            if (carRequestInfoList != null) {
                requestInfoObject.setTotal(count);
                requestInfoObject.setRequestInfo(carRequestInfoList);
                result = ResultTool.success(requestInfoObject);
            } else {
                result = ResultTool.error();
                result.setMessage("查询不到申请记录！");
            }
            return result;
        } catch (Exception e) {
            result = ResultTool.error();
            result.setMessage("查询失败！");
            return result;
        }

    }
}
