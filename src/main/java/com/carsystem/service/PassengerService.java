package com.carsystem.service;
import com.carsystem.dao.SystemCarMapper;
import com.carsystem.dao.SystemRequestMapper;
import com.carsystem.dao.SystemUserMapper;
import com.carsystem.model.OV.ResultCode;
import com.carsystem.model.ShortMessage.RequestRemind;
import com.carsystem.model.jsonrequestbody.OrderCarJson;
import com.carsystem.model.jsonrequestbody.RequestIdJson;
import com.carsystem.model.responsebody.*;
import com.carsystem.model.OV.Result;
import com.carsystem.model.OV.ResultTool;
import com.carsystem.model.jsonrequestbody.QueryJson;
import com.carsystem.model.entity.*;
import com.carsystem.tools.MessageTool;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author xw
 */
@Service
public class PassengerService {
    @Resource
    private SystemRequestMapper systemRequestMapper;
    @Resource
    private UserService userService;
    @Resource
    private CarService carService;
    @Resource
    private SystemUserMapper systemUserMapper;
    @Resource
    private SystemCarMapper carMapper;

    public Result<List<AvailableCarResponse>> getAvailableCar(Long beginTime, Long endTime) {
        Result<List<AvailableCarResponse>> result = new Result<>();
        List<SystemCar> list = carMapper.getAvaliableCar(beginTime, endTime);
        List<AvailableCarResponse> responseList = new ArrayList<>();
        for (SystemCar temp : list){
            responseList.add(new AvailableCarResponse(temp));
        }
        result.setCode(ResultCode.SUCCESS);
        result.setData(responseList);
        return result;
    }


    /**
     * 查询乘客个人信息
     *
     * @param userId
     * @return 返回封装后的Result对象
     */
    public Result queryPassengerInfo(String userId) {
        //1.查询符合条件的数据
        SystemUser systemUser = userService.queryUserInfo(userId);
        //2.数据拼接
        PassengerInfo passengerInfo = new PassengerInfo();
        passengerInfo.setUserId(systemUser.getUserId());
        passengerInfo.setUserName(systemUser.getUserName());
        passengerInfo.setUserPhone(systemUser.getUserPhone());
        passengerInfo.setUserImg(systemUser.getUserImg());
        passengerInfo.setDepartment(systemUser.getDepartment());
        passengerInfo.setDuty(systemUser.getDuty());
        Result result = new Result<>();
        if (passengerInfo != null) {
            result = ResultTool.success(passengerInfo);
        } else {
            result = ResultTool.error();
            result.setMessage("找不到用户信息！");
        }
        return result;
    }

    /**
     * @Description: 提交预约申请
     * @Param:
     * @Return:
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result OrderCar(String userId,OrderCarJson orderCarJson)throws ParseException {
        Result result = new Result();
        // 日期数据格式化为Long型数据，例：201802230118
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        Long startTime = Long.parseLong(sdf.format(sdf.parse(orderCarJson.getStartDate())));
        Long endTime = Long.parseLong(sdf.format(sdf.parse(orderCarJson.getEndDate())));
        SystemCar systemCar = carMapper.selectByPrimaryKey(orderCarJson.getCarId());
        SystemRequest systemRequest = new SystemRequest();
        //查找出所有管理员
        SystemUserExample example = new SystemUserExample();
        example.createCriteria()
                .andUserIdentityEqualTo((byte) 3);
        List<SystemUser> adminList = systemUserMapper.selectByExample(example);

        try{
            systemRequest.setPassengerId(userId);
            systemRequest.setCarId(orderCarJson.getCarId());
            systemRequest.setBeginTime(startTime);
            systemRequest.setEndTime(endTime);
            systemRequest.setPlace(orderCarJson.getPlace());
            systemRequest.setRequestReason(orderCarJson.getRequestReason());
            systemRequest.setDriverId(systemCar.getDriverId());
            systemRequest.setRequestState((byte) 1);
            systemRequestMapper.insert(systemRequest);
            result = ResultTool.success();
            try{
                for(SystemUser admin : adminList){
                    //向管理员发送预约提交提醒短信
                    RequestRemind requestRemind = new RequestRemind();
                    // 日期数据格式化为Long型数据，例：201802230118
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmm");
                    Long date = Long.parseLong(sdf1.format(new Date()));
                    requestRemind.setPassengerId(userId);
                    requestRemind.setRequestTime(MessageTool.transferLongDateToString(date));
                    MessageTool.RemindMessageToAdmin(requestRemind,admin.getUserPhone());
                }
            }catch (Exception e){
                result = ResultTool.error();
                result.setMessage("短信发送失败");
            }


        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("预约失败");
        }
        return result;
    }

    /**
     * 用户查询预约申请信息
     *
     * @param userId
     * @return 返回符合查询条件的预约申请列表
     */
    public RequestObject queryRequest(String userId, QueryJson queryJson) {

        Integer type = queryJson.getType();
        Integer page = queryJson.getPage();
        RequestObject requestObject = new RequestObject();
        /*获取当前的日期*/
        Date date = new Date();
        /*设置时间格式*/
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        /*将时间，类型转化为Long时间*/
        String createdate = sdf.format(date);
        Long time = Long.parseLong(createdate);
        try {
            if (type == 0) {
                /*查询用户所有请求*/
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andPassengerIdEqualTo(userId);
                SystemRequestExample example1 = new SystemRequestExample();
                example1.createCriteria()
                        .andPassengerIdLike(userId);
                int num = systemRequestMapper.countByExample(example1);
                requestObject.setTotal(num);
                List<SystemRequest> systemRequest = systemRequestMapper.selectByExample((page-1)*10,example);
                requestObject.setRequestInfo(systemRequestMapper.selectByExample((page-1)*10,example));

                return requestObject;

            } else if (type == 1) {
                /*查询未到用车时间的请求*/
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andPassengerIdEqualTo(userId)
                        .andBeginTimeGreaterThan(time);
                requestObject.setTotal(systemRequestMapper.countByExample(example));
                requestObject.setRequestInfo(systemRequestMapper.selectByExample((page-1)*10,example));
                return requestObject;

            } else if (type == 2) {
                /*查询正在生效的预约请求*/
                Byte success = 3; //预约申请状态为审核通过
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andPassengerIdEqualTo(userId)
                        .andBeginTimeLessThanOrEqualTo(time)
                        .andEndTimeGreaterThanOrEqualTo(time)
                        .andRequestStateEqualTo(success);
                requestObject.setTotal(systemRequestMapper.countByExample(example));
                requestObject.setRequestInfo(systemRequestMapper.selectByExample((page-1)*10,example));
                return requestObject;
            } else if (type == 3) {
                /*查询已失效的预约申请*/
                Byte success = 3; //预约申请状态为审核通过
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andPassengerIdEqualTo(userId)
                        .andEndTimeLessThan(time)
                        .andRequestStateEqualTo(success);
                requestObject.setTotal(systemRequestMapper.countByExample(example));
                requestObject.setRequestInfo(systemRequestMapper.selectByExample((page-1)*10,example));
                return requestObject;
            } else {
                /*审核未通过的预约*/
                Byte fail = 4;//预约申请状态未审核被驳回
                SystemRequestExample example = new SystemRequestExample();
                example.createCriteria()
                        .andPassengerIdEqualTo(userId)
                        .andRequestStateEqualTo(fail);
                requestObject.setTotal(systemRequestMapper.countByExample(example));
                requestObject.setRequestInfo(systemRequestMapper.selectByExample((page-1)*10,example));
                return requestObject;
            }
        }catch (Exception e) {
            return null;
        }
    }

    /**
     * 查讯乘客相关的预约情求
     *
     * @param userId
     * @return 返回封装好后Result对象
     */
    public Result queryRequestInfo(String userId, QueryJson queryJson) {

        //1.查询符合条件的数据
        RequestObject requestList = queryRequest(userId, queryJson);
        SystemUser systemUser = userService.queryUserInfo(userId);
        //2.数据拼装
        List<RequestInfo> requestInfoList = new ArrayList<>();
        if(requestList!=null){
            for (SystemRequest request : requestList.getRequestInfo()) {
                //用于获取汽车相关信息
                SystemCar systemCar = carService.queryCarInfo(request.getCarId());
                //用于获取司机用户的电话
                SystemUser systemUser1 = systemUserMapper.selectByPrimaryKey(request.getDriverId());
                RequestInfo requestInfo = new RequestInfo();
                requestInfo.setBeginTime(MessageTool.transferLongDateToString(request.getBeginTime()));
                requestInfo.setEndTime(MessageTool.transferLongDateToString(request.getEndTime()));
                requestInfo.setRequestState(request.getRequestState());
                requestInfo.setRequestReason(request.getRequestReason());
                requestInfo.setUserId(request.getPassengerId());
                requestInfo.setUserName(systemUser.getUserName());
                requestInfo.setRequestId(request.getRequestId());
                requestInfo.setUserImg(systemUser.getUserImg());
                requestInfo.setDriverName(systemCar.getDriverName());
                requestInfo.setDriverPhone(systemUser1.getUserPhone());
                requestInfo.setCarNumber(systemCar.getCarNumber());
                requestInfo.setCarModel(systemCar.getCarModel());
                requestInfo.setCarSize(systemCar.getCarSize());
                requestInfo.setCarId(systemCar.getCarId());
                requestInfo.setPlace(request.getPlace());
                requestInfoList.add(requestInfo);
            }
        }else{
            requestInfoList = null;
        }
        Result result = new Result<>();
        RequestInfoObject requestInfoObject = new RequestInfoObject();
        requestInfoObject.setTotal(requestList.getTotal());
        requestInfoObject.setRequestInfo(requestInfoList);
        try{
            if (requestInfoList != null)
            {
                result = ResultTool.success(requestInfoObject);
            }
            else {
                result = ResultTool.error();
                result.setMessage("找不到预约记录！");
            }
        }catch (Exception E){
            result = ResultTool.error();
            result.setMessage("查询失败！");
        }
        return result;
    }

    /**
     * 取消正在申请的预约
     *
     * @param userId
     * @param requestIdJson
     * @return 返回封装好的Result对象
     */
    public Result deleteRequest(String userId, RequestIdJson requestIdJson) {
        byte delete = 2;
        Long requestId = requestIdJson.getRequestId();
        SystemRequestExample example = new SystemRequestExample();
        Result result = new Result();
        try{
            example.createCriteria()
                    .andPassengerIdEqualTo(userId)
                    .andRequestIdEqualTo(requestId);
            SystemRequest request = systemRequestMapper.selectByPrimaryKey(requestId);
            request.setRequestState(delete);
            int res = systemRequestMapper.updateByExample(request, example);
            if (res != 0) {
                result = ResultTool.success();
                result.setMessage("取消成功");
            } else {
                result = ResultTool.error();
                result.setMessage("取消失败");
            }
            return result;
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("取消失败");
            return result;
        }

    }
}
