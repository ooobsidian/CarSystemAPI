package com.carsystem.service;

import com.aliyuncs.exceptions.ClientException;
import com.carsystem.dao.*;
import com.carsystem.model.OV.Result;
import com.carsystem.model.OV.ResultTool;
import com.carsystem.model.ShortMessage.CarRequestMessage;
import com.carsystem.model.ShortMessage.FailMessage;
import com.carsystem.model.ShortMessage.SuccessMessage;
import com.carsystem.model.entity.*;
import com.carsystem.model.jsonrequestbody.*;
import com.carsystem.model.responsebody.*;
import com.carsystem.tools.MessageTool;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {
    @Resource
    private SystemUserMapper systemUserMapper;

    @Resource
    private SystemPassengerMapper systemPassengerMapper;

    @Resource
    private SystemDriverMapper systemDriverMapper;

    @Resource
    private SystemCarMapper systemCarMapper;

    @Resource
    private SystemCarRequestMapper systemCarRequestMapper;

    @Resource
    private SystemRequestMapper systemRequestMapper;


    /**
     * @Description: 管理员查询所有预约申请
     * @Param: page
     * @Return: carResquest
     * @Author: xw
     * @Date: 18-11-24
     */
    public Result getRequestInfo(Integer carId){
        Result result = new Result();
        try{
            SystemRequestExample example = new SystemRequestExample();
            example.createCriteria()
                    .andCarIdEqualTo(carId)
                    .andRequestStateEqualTo((byte) 1);
            List<SystemRequest> requestList = systemRequestMapper.select(example);
            result = ResultTool.success(requestList);
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询出错!");
        }
        return result;

    }

    /**
     * @Description: 管理员查询所有司机请假申请
     * @Param: page
     * @Return: carResquest
     * @Author: xw
     * @Date: 18-11-24
     */
    public Result getCarRequestInfo(QueryJson queryJson){
        Result result = new Result();
        Integer state = queryJson.getType();
        List request = new ArrayList();
        try{
            if(queryJson.getType() == 1){
                //查询待审核的预约申请
                SystemCarRequestExample example = new SystemCarRequestExample();
                example.createCriteria()
                        .andCarRequestStateEqualTo((byte)1);
                request = systemCarRequestMapper.selectByExample(example,(queryJson.getPage()-1)*10);
            }else if(queryJson.getType() == 2){
                //查询审核通过的预约申请
                SystemCarRequestExample example = new SystemCarRequestExample();
                example.createCriteria()
                        .andCarRequestStateEqualTo((byte)3);
                request = systemCarRequestMapper.selectByExample(example,(queryJson.getPage()-1)*10);
            }else if(queryJson.getType() == 3){
                //查询审核通过的预约申请
                SystemCarRequestExample example = new SystemCarRequestExample();
                example.createCriteria()
                        .andCarRequestStateEqualTo((byte)4);
                request = systemCarRequestMapper.selectByExample(example,(queryJson.getPage()-1)*10);
            }else{
                result = ResultTool.error();
                result.setMessage("查询失败!");
            }
            result = ResultTool.success(request);
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询出错!");
        }
        return result;

    }
    
    /**
     * @Description: 审核用户预约请求
     * @Param: 
     * @Return: 
     * @Author: xw
     * @Date: 18-11-24
     */
    public Result checkRequest(Long requestId,int requestState) throws ClientException {
        Result result = new Result();
        try{
            SystemRequest systemRequest = systemRequestMapper.selectByPrimaryKey(requestId);
            systemRequest.setRequestState((byte)requestState);
            SystemUser passenger = systemUserMapper.selectByPrimaryKey(systemRequest.getPassengerId());
            SystemUser driver = systemUserMapper.selectByPrimaryKey(systemRequest.getDriverId());
            if(requestState == 3){ //审核通过用户预约申请
                //向用户发送预约成功的短信
                SuccessMessage successMessage = new SuccessMessage();
                successMessage.setPassengerName(passenger.getUserName());
                successMessage.setDriverName(driver.getUserName());
                successMessage.setPlace(systemRequest.getPlace());
                successMessage.setBeginTime(MessageTool.transferLongDateToString(systemRequest.getBeginTime()));
                successMessage.setEndTime(MessageTool.transferLongDateToString(systemRequest.getEndTime()));
                successMessage.setDriverPhone(driver.getUserPhone());
                successMessage.setPassengerPhone(passenger.getUserPhone());
                MessageTool.successMessageToPassenger(successMessage, passenger.getUserPhone());
                result = ResultTool.success();
                result.setMessage("申请已经通过!");
            }else if(requestState == 4){ //审核驳回用户预约申请
                //向用户发送审核驳回提醒短信
                FailMessage failMessage = new FailMessage();
                failMessage.setPassengerName(passenger.getUserName());
                failMessage.setBeginTime(MessageTool.transferLongDateToString(systemRequest.getBeginTime()));
                failMessage.setEndTime(MessageTool.transferLongDateToString(systemRequest.getEndTime()));
                failMessage.setPassengerPhone(passenger.getUserPhone());
                MessageTool.FailMessageToPassenger(failMessage,passenger.getUserPhone());
                result = ResultTool.error();
                result.setMessage("申请驳回成功!");
            }else{
                result = ResultTool.error();
                result.setMessage("操作无效!");
            }
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("审核失败!");
        }
        return result;
    }

    /**
     * @Description: 审核司机请假申请
     * @Param: CarRequestId
     * @Return:
     * @Author: xw
     * @Date: 18-11-24
     */
    public Result checkCarRequest(Long carRequestId, int carRequestState) throws ClientException{
        Result result = new Result();
        try{
            SystemCarRequest systemCarRequest = systemCarRequestMapper.selectByPrimaryKey(carRequestId);
            systemCarRequest.setCarRequestState((byte)carRequestState);
            SystemUser driver = systemUserMapper.selectByPrimaryKey(systemCarRequest.getDriverId());
            /*设置时间格式*/
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
            /*将时间，类型转化为Long时间*/
            String createdate = sdf.format(systemCarRequest.getCarRequestTime());
            Long time = Long.parseLong(createdate);
            if(carRequestState == 3){
                //向司机发送审核通过短信
                CarRequestMessage carRequestMessage = new CarRequestMessage();
                carRequestMessage.setDriverName(driver.getUserName());
                carRequestMessage.setCarId(systemCarRequest.getCarId());
                carRequestMessage.setDate(MessageTool.transferLongDateToString(time));
                MessageTool.SuccessCarMessageToAdmin(carRequestMessage,"18817602319");
                result = ResultTool.success();
            }else if(carRequestState == 4){
                //向司机发送审核不通过短信
                CarRequestMessage carRequestMessage = new CarRequestMessage();
                carRequestMessage.setDriverName(driver.getUserName());
                carRequestMessage.setCarId(systemCarRequest.getCarId());
                carRequestMessage.setDate(MessageTool.transferLongDateToString(time));
                MessageTool.FailCarMessageToAdmin(carRequestMessage,"18817602319");
                result = ResultTool.success();
            }else{
                result = ResultTool.error();
                result.setMessage("操作无效!");
            }
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("审核失败!");
        }
        return result;
    }

    /**
     * @Description: 添加乘客信息
     * @Param:
     * @Return:
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result AddPassengerInfo(AddPassengerJson addPassengerJson){
        SystemUser systemUser = new SystemUser();
        SystemPassenger systemPassenger = new SystemPassenger();
        Result result = new Result();
        try{
            systemUser.setUserId(addPassengerJson.getUserId());
            systemUser.setUserName(addPassengerJson.getUserName());
            systemUser.setUserPhone(addPassengerJson.getUserPhone());
            systemUser.setUserIdentity((byte)1);
            systemUser.setUserState((byte) 1);
            systemPassenger.setPassengerId(addPassengerJson.getUserId());
            systemPassenger.setPassengerPriority((byte) 1);
            systemUserMapper.insert(systemUser); //向用户表中插入数据
            systemPassengerMapper.insert(systemPassenger);//向乘客表中插入数据
            result = ResultTool.success();
            result.setMessage("添加成功");
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("添加失败");
        }
        return result;
    }

    /**
     * @Description: 添加司机信息
     * @Param:
     * @Return:
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result AddDriverInfo(AddDriverJson addDriverJson){
        SystemUser systemUser = new SystemUser();
        SystemDriver systemDriver = new SystemDriver();
        Result result = new Result();
        try{
            systemUser.setUserId(addDriverJson.getDriverId());
            systemUser.setUserName(addDriverJson.getDriverName());
            systemUser.setUserPhone(addDriverJson.getDriverPhone());
            systemUser.setUserIdentity((byte)2);
            systemUser.setUserState((byte) 1);
            systemDriver.setDriverId(addDriverJson.getDriverId());
            systemDriver.setDriverState((byte) 1);
            systemUserMapper.insert(systemUser); //向用户表中插入数据
            systemDriverMapper.insert(systemDriver);//向司机表中插入数据
            result = ResultTool.success();
            result.setMessage("添加成功");
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("添加失败");
        }
        return result;
    }

    /**
     * @Description: 添加公车信息
     * @Param:
     * @Return:
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result AddCarInfo(AddCarJson addCarJson){
        SystemCar systemCar = new SystemCar();
        Result result = new Result();
        try{
            systemCar.setCarNumber(addCarJson.getCarNumber());
            systemCar.setCarModel(addCarJson.getModel());
            systemCar.setCarRemark(addCarJson.getCarRemark());
            systemCar.setCarSize(addCarJson.getCarSize());
            systemCar.setDriverId(addCarJson.getDriverId());
            systemCar.setDriverName(addCarJson.getDrivetName());
            systemCar.setCarState((byte) 1);
            systemCarMapper.insert(systemCar);
            result = ResultTool.success();
            result.setMessage("添加成功");
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("添加失败");
        }
        return result;
    }

    /**
     * @Description: 查询乘客信息
     * @Param: 
     * @Return: PassengerInfo
     * @Author: xw
     * @Date: 18-11-25
     */
    public Result queryPassenger(PageJson pageJson){
        Result result = new Result();
        try{
            List<PassengerInfo> passengerInfoList = new ArrayList<>();
            SystemPassengerExample example = new SystemPassengerExample();
            example.createCriteria()
                    .andPassengerIdIsNotNull();
            int count = systemPassengerMapper.countByExample(example);
            List<SystemPassenger> systemPassengerList = systemPassengerMapper.selectByExample(example,(pageJson.getPage()-1)*10);
            for(SystemPassenger systemPassenger : systemPassengerList){
                PassengerInfo passengerInfo = new PassengerInfo();
                SystemUser systemUser = systemUserMapper.selectByPrimaryKey(systemPassenger.getPassengerId());
                passengerInfo.setUserId(systemUser.getUserId());
                passengerInfo.setUserName(systemUser.getUserName());
                passengerInfo.setUserPhone(systemUser.getUserPhone());
                passengerInfo.setDepartment(systemUser.getDepartment());
                if(systemPassenger.getPassengerPriority()==1){
                    passengerInfo.setPassengerState("正常");
                }else{
                    passengerInfo.setPassengerState("冻结");
                }
                passengerInfoList.add(passengerInfo);
            }
            RequestInfoObject requestObject = new RequestInfoObject();
            requestObject.setTotal(count);
            requestObject.setRequestInfo(passengerInfoList);
            result = ResultTool.success(requestObject);
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询失败");
        }
        return result;
    }

    /**
     * @Description: 查询司机信息
     * @Param:
     * @Return: DriverInfo
     * @Author: xw
     * @Date: 18-11-25
     */
    public Result queryDriver(PageJson pageJson){
        Result result = new Result();
        List<DriverInfo> driverInfoList = new ArrayList<>();
        try{
            SystemDriverExample example = new SystemDriverExample();
            example.createCriteria()
                    .andDriverIdIsNotNull();
            int count = systemDriverMapper.countByExample(example);
            List<SystemDriver> systemDriverList = systemDriverMapper.selectByExampleAndPage(example,(pageJson.getPage()-1)*10);
            for(SystemDriver systemDriver: systemDriverList){
                SystemUser systemUser = systemUserMapper.selectByPrimaryKey(systemDriver.getDriverId());
                SystemCar systemCar = systemCarMapper.selectByDriverId(systemDriver.getDriverId());
                DriverInfo driverInfo = new DriverInfo();
                driverInfo.setUserId(systemUser.getUserId());
                driverInfo.setUserName(systemUser.getUserName());
                driverInfo.setUserPhone(systemUser.getUserPhone());
                driverInfo.setCarId(systemCar.getCarId());
                driverInfo.setCarNumber(systemCar.getCarNumber());
                driverInfo.setDepartment(systemUser.getDepartment());
                driverInfo.setCarModel(systemCar.getCarModel());
                driverInfo.setCarSize(systemCar.getCarSize());
                if(systemDriver.getDriverState()==1){
                    driverInfo.setDriverState("正常");
                }else{
                    driverInfo.setDriverState("不在职");
                }
                driverInfoList.add(driverInfo);
            }
            RequestInfoObject requestObject = new RequestInfoObject();
            requestObject.setTotal(count);
            requestObject.setRequestInfo(driverInfoList);
            result = ResultTool.success(requestObject);
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询失败");
        }
        return result;
    }
    
    /**
     * @Description: 查询公车信息
     * @Param: 
     * @Return: CarInfo
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result queryCarInfo(PageJson pageJson){
        Result result = new Result();
        try{
            List<CarInfo> carInfoList = new ArrayList<>();
            SystemCarExample example = new SystemCarExample();
            example.createCriteria()
                    .andCarIdIsNotNull();
            int count = systemCarMapper.countByExample(example);
            List<SystemCar> systemCarList = systemCarMapper.selectByExampleAndPage(example,(pageJson.getPage()-1)*10);
            for(SystemCar systemCar : systemCarList){
                CarInfo carInfo = new CarInfo();
                carInfo.setCarId(systemCar.getCarId());
                carInfo.setCarModel(systemCar.getCarModel());
                carInfo.setCarNumber(systemCar.getCarNumber());
                carInfo.setCarRemark(systemCar.getCarRemark());
                carInfo.setCarSize(systemCar.getCarSize());
                carInfo.setDriverId(systemCar.getDriverId());
                carInfo.setDriverName(systemCar.getDriverName());
                if(systemCar.getCarState()==1){
                    carInfo.setCarState("正常");
                }else {
                    carInfo.setCarState("正在维修");
                }

                carInfoList.add(carInfo);
            }

            RequestInfoObject requestObject = new RequestInfoObject();
            requestObject.setTotal(count);
            requestObject.setRequestInfo(carInfoList);
            result = ResultTool.success(requestObject);
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("查询失败");
        }
        return result;

    }

    /**
     * @Description: 冻结或解冻乘客
     * @Param: userid, state
     * @Return: result
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result FreezePassenger(FreezePassenger freezePassenger){
        Result result = new Result();
        int num = freezePassenger.getPassengerState();
        Byte state = (byte) num;
        try{
            SystemPassenger systemPassenger = systemPassengerMapper.selectByUserId(freezePassenger.getUserId());
            systemPassenger.setPassengerPriority(state);
            //改变乘客可预约状态
            SystemPassengerExample example = new SystemPassengerExample();
            example.createCriteria()
                    .andPassengerIdEqualTo(freezePassenger.getUserId());
            systemPassengerMapper.updateByExample(systemPassenger,example);
            result = ResultTool.success();
            result.setMessage("操作成功!");
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("操作失败!");
        }
        return result;
    }

    /**
     * @Description: 冻结或解冻司机
     * @Param:
     * @Return:
     * @Author: xw
     * @Date: 18-11-26
     */
    public Result FreezeDriver(FreezeDriver freezeDriver){
        Result result = new Result();
        int num = freezeDriver.getDriverState();
        Byte state = (byte) num;
        try{
            SystemDriver systemDriver = systemDriverMapper.selectByPrimaryKey(freezeDriver.getUserId());
            systemDriver.setDriverState(state);
            //改变司机的状态
            SystemDriverExample example = new SystemDriverExample();
            example.createCriteria()
                    .andDriverIdEqualTo(freezeDriver.getUserId());
            systemDriverMapper.updateByExampleSelective(systemDriver,example);
            //同时改变司机对应的公车的状态
            SystemCarExample example1 = new SystemCarExample();
            example1.createCriteria()
                    .andDriverIdEqualTo(freezeDriver.getUserId());
            SystemCar systemCar = systemCarMapper.selectByDriverId(freezeDriver.getUserId());
            systemCarMapper.updateByExample(systemCar,example1);
            result = ResultTool.success();
            result.setMessage("操作成功");
        }catch (Exception e){
            result = ResultTool.error();
            result.setMessage("操作失败");
        }
        return result;
    }




}
