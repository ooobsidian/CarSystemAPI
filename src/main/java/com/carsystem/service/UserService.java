package com.carsystem.service;


import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.carsystem.dao.SystemRequestMapper;
import com.carsystem.dao.SystemUserMapper;
import com.carsystem.model.LoginResponse.LoginUser;
import com.carsystem.model.LoginResponse.TokenResponse;
import com.carsystem.model.OV.Result;
import com.carsystem.model.OV.ResultCode;
import com.carsystem.model.OV.ResultTool;
import com.carsystem.model.ShortMessage.CodeMessage;
import com.carsystem.model.ShortMessage.ValidateCode;
import com.carsystem.model.entity.SystemPassenger;
import com.carsystem.model.entity.SystemUser;
import com.carsystem.model.entity.SystemUserExample;
import com.carsystem.tools.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;

/**
 * @author xw
 */
@Service
public class UserService {
    @Resource
    private SystemUserMapper userMapper;
    @Resource
    private SystemRequestMapper systemRequestMapper;
    private static final byte LOGIN_ENABLE = 1;
    private static final byte LOGIN_DISABLE = 2;
    private static final String DUTY_TEACHER = "本科生";
    private static final String SEND_CODE_SUCCESS = "OK";
    private static final String KEY = "ShuCarSystem";

    public Result<TokenResponse> login(LoginUser user) {
        Result<TokenResponse> result = new Result<>();
        if (user == null || user.getUserId() == null || "".equals(user.getUserId()) || user.getPassword() == null || "".equals(user.getPassword())) {
            result.setCode(ResultCode.FAILED);
            result.setMessage("账号、密码不能为空");
            return result;
        }
        // 首先验证数据库中有没有该用户
        SystemUser existedUser = userMapper.selectByPrimaryKey(user.getUserId());
        if (existedUser != null) {
            //如果该账户的账号密码验证正确并且可以登录
            if ((user.getUserId().equals(existedUser.getUserId())
                    && MD5Tool.getMD5(user.getPassword()).equals(existedUser.getPassword()))
                    && existedUser.getUserState() == LOGIN_ENABLE) {
                result.setCode(ResultCode.SUCCESS);
                TokenResponse response = new TokenResponse();
                response.setUserId(user.getUserId());
                response.setUserName(existedUser.getUserName());
                response.setImg(existedUser.getUserImg());
                response.setToken(JwtUtil.createJwt(user.getUserId()));
                response.setUserIdentity(existedUser.getUserIdentity());
                result.setData(response);
                return result;
                //如果密码输入错误
            } else if (!(user.getUserId().equals(existedUser.getUserId())
                    && MD5Tool.getMD5(user.getPassword()).equals(existedUser.getPassword()))) {
                return ResultTool.error("密码输入错误");
                //如果该账户登录权限为禁止登陆
            } else {
                return ResultTool.error("您没有权限登录该系统");
            }
        } else {
            //如果管理员已经添加了用户信息且密码字段为空,此时用户再登录则将完整的信息补充到数据库
            // 请求上海大学登陆接口查看有没有该用户，有的话该用户更新到我们的数据库，没有的话返回登陆失败的信息
            if (AuthTool.getAuth(user.getUserId(), user.getPassword())) {
                SystemUser user1 = userMapper.selectByPrimaryKey(user.getUserId());
                SystemUser systemUser = AuthTool.getInfo(user.getUserId());
                systemUser.setCreateTime(new Date());
                systemUser.setUpdateTime(new Date());
                systemUser.setUserId(user.getUserId());
                systemUser.setUserPhone(user1.getUserPhone());
                systemUser.setPassword(MD5Tool.getMD5(user.getPassword()));
                if (systemUser.getDuty().equals(DUTY_TEACHER)) {
                    // 数据进入system_user表
                    systemUser.setUserState(LOGIN_ENABLE);
                    systemUser.setUserIdentity(user1.getUserIdentity());
                    SystemUserExample example = new SystemUserExample();
                    example.createCriteria()
                            .andUserIdEqualTo(user.getUserId());
                    userMapper.updateByExample(systemUser,example);

                    SystemUser systemUser1 = userMapper.selectByPrimaryKey(user.getUserId());
                    // 同时数据进入system_passenger表
                    SystemPassenger passenger = new SystemPassenger();
                    passenger.setPassengerId(user.getUserId());
                    passenger.setPassengerPriority((byte) 1);
                    result.setCode(ResultCode.SUCCESS);
                    TokenResponse response = new TokenResponse();
                    response.setUserId(user.getUserId());
                    response.setUserName(systemUser1.getUserName());
                    response.setImg(systemUser1.getUserImg());
                    response.setToken(JwtUtil.createJwt(user.getUserId()));
                    response.setUserIdentity(systemUser1.getUserIdentity());
                    result.setData(response);
                    return result;
                } else {
                    systemUser.setUserState(LOGIN_DISABLE);
                    userMapper.insertSelective(systemUser);
                    result.setCode(ResultCode.FAILED);
                    result.setMessage("您没有权限使用该系统");
                    return result;
                }
            } else {
                result.setCode(ResultCode.FAILED);
                result.setMessage("您不是上海大学的用户");
                return result;
            }
        }
    }

    /**
     * 用户查询个人信息
     *
     * @param userId
     * @return
     */
    public SystemUser queryUserInfo(String userId) {
        SystemUser systemUser = new SystemUser();
        systemUser = userMapper.selectByPrimaryKey(userId);
        return systemUser;
    }


    /**
     * 给已存在的手机发送验证码
     *
     * @param userId 用户Id
     * @return
     */
    public Result<ValidateCode> sendCodeToExistPhone(String userId) {
        Result<ValidateCode> result = new Result<>();
        SystemUser systemUser = userMapper.selectByPrimaryKey(userId);
        if (systemUser != null) {
            if (systemUser.getUserPhone() != null) {
                ValidateCode validateCode = sendValidateCode(systemUser.getUserPhone());
                checkValidateCode(validateCode, result);
                return result;
            } else {
                result.setCode(ResultCode.FAILED);
                result.setMessage("您还没有预留手机号，请先去上传手机号");
                return result;
            }
        } else {
            result.setCode(ResultCode.FAILED);
            result.setMessage("登陆已失效，请重新登陆");
            return result;
        }
    }

    /**
     * 给新手机发送验证码
     *
     * @param phoneNum 新手机号码
     * @return
     */
    public Result<ValidateCode> sendCodeToNewPhone(String phoneNum) {
        Result<ValidateCode> result = new Result<>();
        ValidateCode code = sendValidateCode(phoneNum);
        checkValidateCode(code, result);
        return result;
    }

    /**
     * 验证码校验并修改手机号
     *
     * @param validateCode
     * @return
     */
    public Result verifyCodeAndUpdatePhone(String userId, ValidateCode validateCode) {
        Result result = verifyCode(validateCode);
        if (result.getCode().equals(ResultCode.SUCCESS)){
            SystemUserExample example = new SystemUserExample();
            example.createCriteria()
                    .andUserIdEqualTo(userId);
            SystemUser user = userMapper.selectByPrimaryKey(userId);
            user.setUserPhone(validateCode.getPhoneNum());
            userMapper.updateByExample(user, example);
        }
        return result;
    }

    /**
     * 验证码校验
     *
     * @param validateCode 验证码
     * @return
     */
    public Result verifyCode(ValidateCode validateCode) {
        Result result = new Result();
        if (validateCode == null) {
            result.setCode(ResultCode.FAILED);
            result.setMessage("缺少必要信息");
            return result;
        }
        if (validateCode.getDeadline() > System.currentTimeMillis()) {
            try {
                String num = SecurityTool.encodeByMd5(KEY + validateCode.getDeadline() + validateCode.getCode());
                if (SecurityTool.encodeByMd5(KEY + validateCode.getDeadline() + validateCode.getCode()).equals(validateCode.getHashKey())) {
                    result.setCode(ResultCode.SUCCESS);
                    return result;
                } else {
                    result.setCode(ResultCode.FAILED);
                    result.setMessage("验证码错误");
                    return result;
                }
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                result.setCode(ResultCode.FAILED);
                result.setMessage(e.getMessage());
                return result;
            }
        } else {
            result.setCode(ResultCode.FAILED);
            result.setMessage("验证码超时，请重新获取");
            return result;
        }

    }

    private void checkValidateCode(ValidateCode code, Result<ValidateCode> result) {
        if (code != null) {
            result.setCode(ResultCode.SUCCESS);
            result.setData(code);
        } else {
            result.setCode(ResultCode.FAILED);
            result.setMessage("发送验证码短信失败，请稍后再试");
        }
    }


    private ValidateCode sendValidateCode(String phoneNum) {
        CodeMessage codeMessage = new CodeMessage();
        String code = MessageTool.generateCode();
        codeMessage.setCode(code);
        Calendar calendar = Calendar.getInstance();
        // 验证码有效期5分钟
        calendar.add(Calendar.MINUTE, 5);
        Long deadline = calendar.getTime().getTime();
        try {
            SendSmsResponse response = MessageTool.sendCode(codeMessage, phoneNum);
            if (response.getCode().equals(SEND_CODE_SUCCESS)) {
                ValidateCode validateCode = new ValidateCode();
                String hash = SecurityTool.encodeByMd5(KEY + deadline + code);
                validateCode.setPhoneNum(phoneNum);
                validateCode.setDeadline(deadline);
                validateCode.setHashKey(hash);
                return validateCode;
            } else {
                return null;
            }
        } catch (ClientException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
            return null;
        }
    }
}
